/* Error codes -*- c -*- */

#ifndef ERROR_H_
# define ERROR_H_

/** @file runtime/Error.h
 * Evaluation error codes from the compiled code
 */

/* Copyright � 2000-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Error codes */
enum Error {
  errNone = 0,	/* no error */
  errConst,	/* constraint violation */
  errVar,	/* undefined variable */
  errUndef,	/* undefined expression evaluated */
  errFatal,	/* fatal expression evaluated */
  errDiv0,	/* division by zero */
  errOver,	/* overflow */
  errMod,	/* modulus error */
  errShift,	/* shift error */
  errUnion,	/* attempt to reference a non-active union component */
  errBuf,	/* buffer boundary violation */
  /* codes not used by the low-level expression evaluator */
  errCard,	/* cardinality overflow */
  errComp	/* incompatible values (no actual error) */
};

#endif /* ERROR_H_ */
