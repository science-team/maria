/** @file runtime/types.h
 * Data types used by the generated code
 */

/* Copyright � 2000-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

#include<limits.h>
#include<sys/types.h>
/** Boolean type (0=false, nonzero=true) */
typedef int bool_t;
/** Cardinality type */
typedef unsigned int card_t;
#define CARD_T_MAX UINT_MAX
#define CARD_T_BIT (CHAR_BIT * sizeof (card_t))
