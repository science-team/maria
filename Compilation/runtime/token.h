/** @file runtime/token.h
 * Definitions for managing tokens in the unification process
 */

/* Copyright � 2000-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** declare a token with bindable variables
 * @param type	type of the associated input place
 * @param num	number of the token
 */
#define TOKEN(type,num)				\
static struct {					\
  card_t count;					\
  card_t reserved;				\
  struct tree##type* m;				\
  struct tree##type* i;				\
} token##num

/** declare a scalar-valued concrete token
 * @param type	type of the associated input place
 * @param num	number of the token
 */
#define TOKENC(type,num)			\
static struct {					\
  card_t count;					\
  card_t reserved;				\
  struct tree##type* m;				\
  struct tree##type* i;				\
  t##type item;					\
} token##num

/** declare a multiset-valued concrete token
 * @param type	type of the associated input place
 * @param num	number of the token
 */
#define TOKENM(type,num)			\
static struct {					\
  struct tree##type* m;				\
  struct tree##type* v;				\
} token##num

/** declare the only token contained in a place
 * @param type	type of the associated input place
 * @param num	number of the token
 */
#define TOKEN1(type,num)			\
static struct {					\
  card_t count;					\
  t##type* item;				\
} token##num

/** initialize a token with bindable variables
 * @param num	number of the token
 * @param place	the associated input place
 */
#define INIT_TOKEN(num,place)			\
do {						\
  token##num.reserved = 0;			\
  token##num.m = place; FIRST (token##num.m);	\
  while (token##num.m && !token##num.m->count)	\
    NEXT (token##num.m);			\
  token##num.i = token##num.m;			\
} while (0)
/** initialize a scalar-valued concrete token
 * @param num	number of the token
 * @param place	the associated input place
 */
#define INIT_TOKENC(num,place) (token##num.reserved = 0, token##num.m = place)
/** initialize a multiset-valued concrete token
 * @param num	number of the token
 * @param place	the associated input place
 */
#define INIT_TOKENM(num,place) (token##num.m = place)

/** reserve a scalar-valued token
 * @param num	number of the token
 */
#define RESERVE_TOKEN(num)						\
(token##num.i->count -= (token##num.reserved = token##num.count))	\
/** release a scalar-valued token
 * @param num	number of the token
 */
#define RELEASE_TOKEN(num)						\
((token##num.i->count += token##num.reserved), token##num.reserved = 0)
/** determine whether there are scalar-valued enough tokens in a place
 * @param num	number of the token
 */
#define ENOUGH_TOKEN(num) (token##num.count <= token##num.i->count)

/** reserve a multiset-valued token
 * @param num	number of the token
 * @param t	type of the token
 */
#define RESERVE_TOKENM(num,t) subtract##t (token##num.m, token##num.v)
/** release a multiset-valued token
 * @param num	number of the token
 * @param t	type of the token
 */
#define RELEASE_TOKENM(num,t) \
(token##num.m = copy##t (token##num.m, token##num.v), FREE (token##num.v))

/** step to the next token
 * @param num	number of the token
 */
#define NEXT_TOKEN(num) NEXT(token##num.i)
/** find a token
 * @param num	number of the token
 * @param t	type of the token
 */
#define FIND_TOKEN(num,t) \
((token##num.i=find##t (token##num.m, &token##num.item)) && ENOUGH_TOKEN (num))

/** find a sub-multiset
 * @param num	number of the token
 * @param t	type of the token
 * @return	true if the token is contained in the input place
 */
#define FIND_TOKENM(num,t) subset##t (token##num.v, token##num.m)
