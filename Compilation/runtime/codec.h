/* coding/decoding base definitions */

#include<stdlib.h>
#include<string.h>

/* determine the endianness */
#if defined __linux
# include <endian.h>
# if !defined BYTE_ORDER
#  define BYTE_ORDER __BYTE_ORDER
#  define LITTLE_ENDIAN __LITTLE_ENDIAN
#  define BIG_ENDIAN __BIG_ENDIAN
# endif /* !defined BYTE_ORDER */
#elif defined _AIX
# include <sys/machine.h>
#elif defined __alpha
# include <machine/endian.h>
#elif defined __sun || defined __hpux
# include <sys/byteorder.h>
# if !defined BYTE_ORDER
#  define LITTLE_ENDIAN 1234
#  define BIG_ENDIAN 4321
#  if !defined _LITTLE_ENDIAN && !defined _BIG_ENDIAN
#   ifdef ntohl
#    define _LITTLE_ENDIAN
#   else /* !defined ntohl */
#    define _BIG_ENDIAN
#   endif /* !defined ntohl */
#  endif /* !defined _LITTLE_ENDIAN && !defined _BIG_ENDIAN */
#  if defined _LITTLE_ENDIAN
#   define BYTE_ORDER LITTLE_ENDIAN
#  elif defined _BIG_ENDIAN
#   define BYTE_ORDER BIG_ENDIAN
#  endif /* little/big endian */
# endif /* !defined BYTE_ORDER */
#elif defined __sgi
# include <standards.h>
# include <sys/endian.h>
#elif !defined BYTE_ORDER
# define LITTLE_ENDIAN 1234
# define BIG_ENDIAN 4321
# if defined __i386
#  define BYTE_ORDER LITTLE_ENDIAN
# else
#  error "cannot determine the byte order"
# endif
#endif

/** @file runtime/codec.h
 * Definitions for encoding and decoding values
 */

/* Copyright � 2000-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/* Machine word */
#if (BYTE_ORDER == BIG_ENDIAN || BYTE_ORDER == LITTLE_ENDIAN)
/** Machine word */
typedef unsigned long word_t;
#else /* mixed-endian */
/** Machine word */
typedef unsigned char word_t;
#endif /* BYTE_ORDER */
#define WORD_T_BIT (CHAR_BIT * sizeof (word_t))

/** number of words a specified amount of bits takes */
#define NUM_WORDS(bits) (((bits) + (WORD_T_BIT - 1)) / WORD_T_BIT)
/** number of bytes a specified amount of bits takes */
#define NUM_BYTES(bits) (((bits) + (CHAR_BIT - 1)) / CHAR_BIT)
/** number of words a specified amount of bytes takes */
#define NUM_WORD_B(bytes) (((bytes) + (sizeof (word_t) - 1)) / sizeof (word_t))

/* determine whether CHAR_BIT is a power of 2 */
#if (CHAR_BIT - 1) & CHAR_BIT
/** Round up a bit amount to the nearest multiple of the machine word length */
# define ROUNDUP_WORDS(bits) (NUM_WORDS(bits) * WORD_T_BIT)
/** Round up a bit amount to the nearest multiple of byte length */
# define ROUNDUP_BYTES(bits) (NUM_BYTES(bits) * CHAR_BIT)
#else
/** Round up a bit amount to the nearest multiple of the machine word length */
# define ROUNDUP_WORDS(bits) (((bits) + (WORD_T_BIT - 1)) & ~(WORD_T_BIT - 1))
/** Round up a bit amount to the nearest multiple of byte length */
# define ROUNDUP_BYTES(bits) (((bits) + (CHAR_BIT - 1)) & ~(CHAR_BIT - 1))
#endif

/** encoding variable */
#define ENCVAR(x) FCN(enc##_##x)
/** decoding variable */
#define DECVAR(x) FCN(dec##_##x)
