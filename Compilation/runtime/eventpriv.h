/** @file runtime/eventpriv.h
 * Definitions for encoding and decoding events
 */

/* Copyright � 2000-2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

#include "types.h"
#include "event.h"
#include "codec.h"
#define FCN(f) f##_##event
#include "codecfcn.h"

volatile char* intr;
char* fatal;
char arcs;
char flat;

void
(*syncstate) (unsigned tr, void* ctx);

void
enc (card_t data, unsigned bits)
{
  FCN (enc) (data, bits);
}

card_t
dec (unsigned bits)
{
  return FCN (dec) (bits);
}

void
event_clear (void)
{
  FCN (clear) ();
}

void
event_cleanup (void)
{
  FCN (cleanup) ();
}

void
inflate (void* buf, size_t bytes) {
  FCN (inflate) ((word_t*) buf, bytes);
}

void*
deflate (size_t* size)
{
  FCN (deflate) ();
  *size = NUM_BYTES (ENCVAR (bits));
  return ENCVAR (buf);
}
