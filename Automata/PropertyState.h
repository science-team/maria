// A state in the property automaton -*- c++ -*-

#ifndef PROPERTYSTATE_H_
# define PROPERTYSTATE_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <assert.h>
# include <limits.h>

/** @file PropertyState.h
 * A state in the property automaton
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** A state in the property automaton */
class PropertyState
{
public:
  /** Default constructor */
  PropertyState () :
    myNumSuccessors (0), mySuccessors (0), myGates (0) {}
private:
  /** Copy constructor */
  explicit PropertyState (const class PropertyState& old);
  /** Assignment operator */
  class PropertyState& operator= (const class PropertyState& old);
public:
  /** Destructor */
  ~PropertyState ();

  /** Determine the number of successor states */
  unsigned getNumSuccessors () const { return myNumSuccessors; }

  /** Access a successor
   * @param i		zero-based index number to the successor
   * @return		the successor
   */
  unsigned operator[] (unsigned i) const {
    assert (i < myNumSuccessors);
    return mySuccessors[i];
  }

  /** Add a successor state
   * @param num		the successor state number
   * @param gate	enabling condition
   */
  void addSuccessor (unsigned num,
		     class Expression& gate);

  /** Evaluate the gates
   * @param valuation	Valuation for evaluating the gates
   * @param result	(out) amount and numbers of enabled successors
   * @param final	number of the final state (UINT_MAX if none)
   * @return		whether the evaluation was successful
   */
  bool eval (const class Valuation& valuation,
	     unsigned*& result,
	     unsigned final = UINT_MAX) const;

# ifdef EXPR_COMPILE
  /** Compile the gate evaluator
   * @param cexpr	the compiled expression
   * @param indent	indentation level
   * @param result	name of the result vector
   * @param final	number of the final state (UINT_MAX if none)
   */
  void compileGates (class CExpression& cexpr,
		     unsigned indent,
		     const char* result,
		     unsigned final) const;
# endif // EXPR_COMPILE

private:
  /** Number of successor states */
  unsigned myNumSuccessors;
  /** The successor states */
  unsigned* mySuccessors;
  /** The successor gates */
  class Expression** myGates;
};

#endif // PROPERTYSTATE_H_
