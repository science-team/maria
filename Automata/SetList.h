// List of sets -*- c++ -*-

#ifndef SETLIST_H_
# define SETLIST_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <assert.h>

/** @file SetList.h
 * A collection of sets of numbers
 */

/* Copyright � 2000-2002 Marko M�kel� (msmakela@tcs.hut.fi).
   Copyright � 2000 Timo Latvala (timo@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** List of sets */
class SetList
{
public:
  /** Constructor
   * @param numSets	number of sets
   */
  explicit SetList (unsigned numSets);
private:
  /** Copy constructor */
  SetList (const class SetList& old);
  /** Assignment operator */
  class SetList& operator= (const class SetList& old);
public:
  /** Destructor */
  ~SetList ();
  /** Assign an array to a set
   * @param i		index number of the set
   * @param array	array to be assigned (length==array[0])
   */
  void assign (unsigned i, unsigned* array);
  /** Assign a bit vector to a set
   * @param i		index number to the set
   * @param bv		BitVector to be assigned
   * @param offset	offset for index numbers
   * @param complement	flag: treat BitVector as its complement
   */
  void assign (unsigned i,
	       const class BitVector& bv,
	       unsigned offset,
	       bool complement);
  /** Copy an array to a set
   * @param i		index number of the set
   * @param array	array to be copied (length==array[0])
   */
  void copy (unsigned i, const unsigned* array);
  /** Grow the list
   * @param numSets	new size of the list
   */
  void grow (unsigned numSets);
  /** Shrink the list
   * @param numSets	new size of the list
   */
  void shrink (unsigned numSets);

  /** Determine the number of sets */
  unsigned getNumSets () const { return myNumSets; }
  /** Determine the size of a set
   * @param i		index number of the set
   * @return		number of elements in the set
   */
  unsigned getSize (unsigned i) const {
    assert (i < myNumSets);
    return mySets[i] ? *mySets[i] : 0;
  }
  /** Access a set
   * @param i		index number of the set
   * @return		pointer to the first element in the set
   */
  const unsigned* operator[] (unsigned i) const {
    assert (i < myNumSets);
    return mySets[i] + 1;
  }
  /** Access a set
   * @param i		index number of the set
   * @return		pointer to the first element in the set
   */
  unsigned* operator[] (unsigned i) {
    assert (i < myNumSets);
    return mySets[i] + 1;
  }
  /** Remove an element of a set by assigning the last element to its place
   * @param i		index number of the set
   * @param elem	index of the element
   */
  void delElement (unsigned i, unsigned elem) {
    assert(i < myNumSets && elem < *mySets[i]);
    mySets[i][elem + 1] = mySets[i][(*mySets[i])--];
  }

private:
  /** Number of sets */
  unsigned myNumSets;
  /** Number of allocated sets */
  unsigned myAllocatedSets;
  /** The sets */
  unsigned** mySets;
};

#endif // SETLIST_H_
