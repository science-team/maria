// Buffer indexing expression (used only during parsing) -*- c++ -*-

#ifdef __GNUC__
# pragma implementation
#endif // __GNUC__
#include "BufferIndex.h"

/** @file BufferIndex.C
 * Buffer indexing expression (transient storage used during parsing)
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

BufferIndex::BufferIndex (class Expression& buffer,
			  class Expression& i) :
  myBuffer (&buffer), myIndex (&i)
{
  assert (myBuffer && myIndex);
  assert (myBuffer->getType ()->getKind () == Type::tBuffer);
  assert (myIndex->getType ()->getKind () == Type::tCard);
  assert (myBuffer->isBasic () && myIndex->isBasic ());

  setType (*myBuffer->getType ());
}

BufferIndex::~BufferIndex ()
{
  myBuffer->destroy ();
  myIndex->destroy ();
}

class Value*
BufferIndex::do_eval (const class Valuation& valuation) const
{
  return myBuffer->eval (valuation);
}

class Expression*
BufferIndex::ground (const class Valuation& valuation,
		     class Transition* transition,
		     bool declare)
{
  return myBuffer->ground (valuation, transition, declare);
}

class Expression*
BufferIndex::substitute (class Substitution& substitution)
{
  return myBuffer->substitute (substitution);
}

bool
BufferIndex::depends (const class VariableSet& vars,
		      bool complement) const
{
  return myBuffer->depends (vars, complement);
}

bool
BufferIndex::forExpressions (bool (*operation)
			     (const class Expression&,void*),
			     void* data) const
{
  return
    (*operation) (*this, data) &&
    myBuffer->forExpressions (operation, data);
}

#ifdef EXPR_COMPILE

void
BufferIndex::compile (class CExpression& cexpr,
		      unsigned indent,
		      const char* lvalue,
		      const class VariableSet* vars) const
{
  myBuffer->compile (cexpr, indent, lvalue, vars);
}

#endif // EXPR_COMPILE

void
BufferIndex::display (const class Printer& printer) const
{
  myBuffer->display (printer);
}
