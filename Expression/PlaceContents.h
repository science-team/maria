// Dynamic place contents -*- c++ -*-

#ifndef PLACECONTENTS_H_
# define PLACECONTENTS_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file PlaceContents.h
 * Operation for reading the marking of a place
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Place contents expression */
class PlaceContents : public Expression
{
public:
  /** Constructor
   * @param place	the place whose marking is to be determined
   */
  PlaceContents (const class Place& place);
private:
  /** Copy constructor */
  PlaceContents (const class PlaceContents& old);
  /** Assignment operator */
  class PlaceContents& operator= (const class PlaceContents& old);
protected:
  /** Destructor */
  ~PlaceContents ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return ePlaceContents; }
  /** Get the place */
  const class Place& getPlace () const { return myPlace; }

  /**
   * Determine whether this is a basic expression containing
   * no temporal logic or set operations
   * @return	true if this is a basic expression
   */
  bool isBasic () const { return false; }
  /**
   * Determine whether this is a temporal logic expression
   * @return	true if this is a temporal logic expression
   */
  bool isTemporal () const { return false; }
  /**
   * Determine whether this is a multiset-valued expression
   * @return	true if this is a multiset-valued expression
   */
  bool isSet () const { return true; }

  /** Equality comparison operator */
  bool operator== (const class PlaceContents& other) const {
    return &myPlace == &other.myPlace;
  }

  /** Ordering comparison operator */
  bool operator< (const class PlaceContents& other) const {
    return &myPlace < &other.myPlace;
  }

  /** Evaluate the multiset expression
   * @param valuation	variable substitutions and the global marking
   * @return		the marking of myPlace
   */
  class PlaceMarking* meval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation&,
			    class Transition*, bool) { return copy (); }

  /** Substitute some variables in the expression with expressions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution&) { return copy (); }

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

# ifdef EXPR_COMPILE
  /** Generate C code for evaluating the expression */
  void compile (class CExpression&,
		unsigned,
		const char*,
		const class VariableSet*) const {
    assert (false);
  }

  /** Generate C code for evaluating a multi-set expression as a scalar
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param result	scalar to assign the multi-set to (must be singleton)
   * @param vars	the variables that have been assigned a value
   * @param check	flag: check for result overflow
   */
  void compileScalarMset (class CExpression& cexpr,
			  unsigned indent,
			  const char* result,
			  const class VariableSet* vars,
			  bool check) const;

  /** Generate C code for evaluating the multi-set expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param resulttype	type of result (optional typecast qualifier)
   * @param result	multi-set to add items to
   * @param vars	the variables that have been assigned a value
   */
  void compileMset (class CExpression& cexpr,
		    unsigned indent,
		    const char* resulttype,
		    const char* result,
		    const class VariableSet* vars) const;

  /** Get the name of the compiled multi-set
   * @param cexpr	the compilation
   * @return		C expression referring to the multi-set
   */
  char* getName (const class CExpression& cexpr) const;
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The place */
  const class Place& myPlace;
};

#endif // PLACECONTENTS_H_
