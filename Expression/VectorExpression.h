// Maria vector expression class -*- c++ -*-

#ifndef VECTOREXPRESSION_H_
# define VECTOREXPRESSION_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"
# include "VectorType.h"

# include <assert.h>

/** @file VectorExpression.h
 * Vector constructor
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Vector initialization expression */
class VectorExpression : public Expression
{
public:
  /** Constructor
   * @param type	Type of the expression
   */
  VectorExpression (const class VectorType& type);
private:
  /** Copy constructor */
  VectorExpression (const class VectorExpression& old);
  /** Assignment operator */
  class VectorExpression& operator= (const class VectorExpression& old);
protected:
  /** Destructor */
  ~VectorExpression ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eVector; }

  /** Set the type of the expression
   * @param type	Type of the expression
   */
  void setType (const class Type& type);

  /** Determine whether this is a basic expression */
  bool isBasic () const { return true; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }

  /** Address a component by index */
  const class Expression& operator[] (card_t i) const {
    return *myComponents[i];
  }
  /** Assign to a component
   * @param i		Index to the component
   * @param component	The component expression
   */
  void setComponent (card_t i, class Expression& component) {
    assert (!myComponents[i]);
    assert (component.isBasic ());
    (myComponents[i] = &component)->setType
      (static_cast<const class VectorType*>(getType ())->getItemType ());
  }

  /** Equality comparison operator */
  bool operator== (const class VectorExpression& other) const {
    const class VectorType* v =
      static_cast<const class VectorType*>(getType ());
    const class VectorType* u =
      static_cast<const class VectorType*>(other.getType ());
    if (v->getSize () != u->getSize ())
      return false;
    for (card_t i = v->getSize (); i--; ) {
      assert (myComponents[i] && other.myComponents[i]);
      if (!(*(myComponents[i]) == *(other.myComponents[i])))
	return false;
    }

    return true;
  }

  /** Ordering comparison operator */
  bool operator< (const class VectorExpression& other) const {
    const class VectorType* v =
      static_cast<const class VectorType*>(getType ());
    const class VectorType* u =
      static_cast<const class VectorType*>(other.getType ());
    if (v->getSize () < u->getSize ())
      return true;
    if (u->getSize () < v->getSize ())
      return false;
    for (card_t i = v->getSize (); i--; ) {
      assert (myComponents[i] && other.myComponents[i]);
      if (*(myComponents[i]) < *(other.myComponents[i]))
	return true;
      else if (*(other.myComponents[i]) < *(myComponents[i]))
	return false;
    }

    return false;
  }

  /** Evaluate the expression
   * @param valuation	Variable substitutions
   * @return		Value of the expression, or NULL in case of error
   */
  class Value* do_eval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @param valuation	Variable substitutions
   * @param transition	Transition for registering quantified variables
   * @param declare	flag: declare new variables if required
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation& valuation,
			    class Transition* transition,
			    bool declare);

  /** Substitute some variables in the expression with expressions
   * @param substitution	Variable substitutions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution& substitution);

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

  /** Determine whether the expression is compatible with the specified value,
   * neglecting subexpressions that cannot be evaluated
   * @param value	value the expression will be compared to
   * @param valuation	variable substitutions
   * @return		true if the expression is compatible with the value
   */
  bool isCompatible (const class Value& value,
		     const class Valuation& valuation) const;

  /** Unify variables from this expression
   * @param value	the value the expression should evaluate to
   * @param valuation	variable substitutions
   * @param vars	the variables to unify
   */
  void getLvalues (const class Value& value,
		   class Valuation& valuation,
		   const class VariableSet& vars) const;

  /** Determine which variables could be unified from this expression
   * @param rvalues	variables unified so far
   * @param lvalues	(output) variables that could be unified
   * @return		variables that could be unified
   */
  void getLvalues (const class VariableSet& rvalues,
		   class VariableSet*& lvalues) const;

# ifdef EXPR_COMPILE
  /** Generate lvalue gathering code
   * @param cexpr	the compilation
   * @param indent	level of indentation
   * @param vars	the variables
   * @param lvalue	C expression referring to the value
   */
  void compileLvalue (class CExpression& cexpr,
		      unsigned indent,
		      const class VariableSet& vars,
		      const char* lvalue) const;

  /** Generate compatibility check code
   * @param cexpr	the compilation
   * @param indent	level of indentation
   * @param vars	the variables that have been unified
   * @param value	C expression referring to the desired value
   */
  void compileCompatible (class CExpression& cexpr,
			  unsigned indent,
			  const class VariableSet& vars,
			  const char* value) const;

  /** Generate C code for evaluating the expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param lvalue	C expression referring to the lvalue
   * @param vars	the variables that have been assigned a value
   */
  void compile (class CExpression& cexpr,
		unsigned indent,
		const char* lvalue,
		const class VariableSet* vars) const;
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The expressions for the vector components */
  class Expression** myComponents;
};

#endif // VECTOREXPRESSION_H_
