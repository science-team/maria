// Maria temporal unary operator class -*- c++ -*-

#ifndef TEMPORALUNOP_H_
# define TEMPORALUNOP_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file TemporalUnop.h
 * Temporal unary operators
 */

/* Copyright � 1998-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Temporal unary operator expression */
class TemporalUnop : public Expression
{
public:
  /** Temporal unary operators */
  enum Op { Finally, Globally, Next };

  /** Constructor
   * @param op		Operator
   * @param expr	Subexpression of the operator
   */
  TemporalUnop (enum Op op,
		class Expression& expr);

private:
  /** Copy constructor */
  TemporalUnop (const class TemporalUnop& old);
  /** Assignment operator */
  class TemporalUnop& operator= (const class TemporalUnop& old);
protected:
  /** Destructor */
  ~TemporalUnop ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eTemporalUnop; }

  /** Determine whether this is a basic expression */
  bool isBasic () const { return false; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return true; }

  /** Equality comparison operator */
  bool operator== (const class TemporalUnop& other) const {
    return myOp == other.myOp && *myExpr == *other.myExpr;
  }

  /** Ordering comparison operator */
  bool operator< (const class TemporalUnop& other) const {
    if (myOp < other.myOp) return true;
    if (other.myOp < myOp) return false;
    return *myExpr < *other.myExpr;
  }

  /** Partially evaluate the expression using a valuation
   * @param valuation	Variable substitutions
   * @param transition	Transition for registering quantified variables
   * @param declare	flag: declare new variables if required
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation& valuation,
			    class Transition* transition,
			    bool declare);

  /** Substitute some variables in the expression with expressions
   * @param substitution	Variable substitutions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution& substitution);

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

  /** Translate the expression to a list of temporal logic connectives
   * and Boolean propositions
   * @param property	the property automaton
   * @return		the translated object
   */
  class Ltl* toFormula (class Property& property);

# ifdef EXPR_COMPILE
  /** Generate C code for evaluating the expression */
  void compile (class CExpression&,
		unsigned,
		const char*,
		const class VariableSet*) const {
    assert (false);
  }
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The operator */
  enum Op myOp;
  /** The subexpression */
  class Expression* myExpr;
};

#endif // TEMPORALUNOP_H_
