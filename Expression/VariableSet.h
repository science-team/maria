// set of variables -*- c++ -*-

#ifndef VARIABLESET_H_
# define VARIABLESET_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <set>

/** @file VariableSet.h
 * Set of variables, a transient class used in preprocessing
 */

/* Copyright � 2000-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Set of variables */
class VariableSet
{
public:
  /** Variable set */
  typedef std::set<const class VariableDefinition*> Set;
  /** Constant iterator to the variable set */
  typedef Set::const_iterator const_iterator;
  /** Constructor */
  VariableSet () : mySet () {}
  /** Copy constructor */
  explicit VariableSet (const class VariableSet& old) : mySet (old.mySet) {}
private:
  /** Assignment operator */
  class VariableSet& operator= (const class VariableSet& old);
public:
  /** Destructor */
  ~VariableSet () {}

  /** Determine whether the set contains a variable
   * @param var		variable to search
   * @return		whether the set contains the variable
   */
  bool contains (const class VariableDefinition& var) const;
  /** Insert a variable to the set
   * @param var		variable to insert
   */
  void insert (const class VariableDefinition& var);

  /** @name Accessors to the variable set */
  /*@{*/
  void clear () { mySet.clear (); }
  bool empty () const { return mySet.empty (); }
  size_t size () const { return mySet.size (); }
  const_iterator begin () const { return mySet.begin (); }
  const_iterator end () const { return mySet.end (); }
  /*@}*/

private:
  /** The set of variables */
  Set mySet;
};

#endif // VARIABLESET_H_
