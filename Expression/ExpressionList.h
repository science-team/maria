// Expression list class -*- c++ -*-

#ifndef EXPRESSIONLIST_H_
# define EXPRESSIONLIST_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "typedefs.h"
# include <assert.h>

/** @file ExpressionList.h
 * List of expressions
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** List of expressions of same type */
class ExpressionList
{
public:
  /** Constructor */
  ExpressionList ();

private:
  /** Copy constructor */
  ExpressionList (const class ExpressionList& old);
  /** Assignment operator */
  class ExpressionList& operator= (const class ExpressionList& old);
protected:
  /** Destructor */
  ~ExpressionList ();
public:
  /** Get a shared copy of the expression list */
  class ExpressionList* copy () { myReferences++; return this; }
  /** Destructs ExpressionList, provided there aren't any references to it */
  void destroy () { if (this && !--myReferences) delete this; }
# ifndef NDEBUG
  /** Determine whether this is the last copy of an expression list */
  bool isLastCopy () const { return myReferences == 1; }
# endif // NDEBUG

  /** Append an expression to the list */
  void append (class Expression& expression);
  /** Prepend an expression to the list */
  void prepend (class Expression& expression);

  /** Get the type associated with the expression list */
  const class Type* getType () const;

  /**
   * Determine whether this is a basic expression list containing
   * no temporal logic or set operations
   * @return	true if this is a basic expression
   */
  bool isBasic () const;
  /**
   * Determine whether this is a temporal logic expression
   * @return	true if this is a temporal logic expression
   */
  bool isTemporal () const;
  /**
   * Determine whether this is a multiset-valued expression
   * @return	true if this is a multiset-valued expression
   */
  bool isSet () const;

  /** Get the number of expressions in the list */
  card_t size () const { return mySize; }
  /** Determine whether the expression list is empty */
  bool empty () const { return !mySize; }

  /** Get an expression from the list
   * @param i	index to the expression
   * @return	the expression
   */
  const class Expression& operator[] (card_t i) const {
    assert (i < size ());
    return *myExprs[i];
  }
  /** Get an expression from the list
   * @param i	index to the expression
   * @return	the expression
   */
  class Expression& operator[] (card_t i) {
    assert (i < size ());
    return *myExprs[i];
  }

  /** Equality comparison operator */
  bool operator== (const class ExpressionList& other) const;

  /** Ordering comparison operator */
  bool operator< (const class ExpressionList& other) const;

  /** Partially evaluate the expression list using a valuation
   * @param valuation	Variable substitutions
   * @param transition	Transition for registering quantified variables
   * @param declare	flag: declare new variables if required
   * @return		grounded expression list, or NULL in case of error
   */
  class ExpressionList* ground (const class Valuation& valuation,
				class Transition* transition,
				bool declare);

  /** Substitute some variables in the expression list with expressions
   * @param substitution	Variable substitutions
   * @return			substituted expression
   */
  class ExpressionList* substitute (class Substitution& substitution);

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

private:
  /** Number of references to the expression list */
  unsigned myReferences;
  /** Number of list members */
  card_t mySize;
  /** The list of expressions */
  class Expression** myExprs;
};

#endif // EXPRESSIONLIST_H_
