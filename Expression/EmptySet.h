// Empty set expression -*- c++ -*-

#ifndef EMPTYSET_H_
# define EMPTYSET_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file EmptySet.h
 * Literal for empty multi-sets
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Empty multi-set expression */
class EmptySet : public Expression
{
public:
  /** Constructor */
  EmptySet () {}
private:
  /** Copy constructor */
  EmptySet (const class EmptySet& old);
  /** Assignment operator */
  class EmptySet& operator= (const class EmptySet& old);
protected:
  /** Destructor */
  ~EmptySet () {}
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eEmptySet; }

  /**
   * Determine whether this is a basic expression containing
   * no temporal logic or set operations
   * @return	true if this is a basic expression
   */
  bool isBasic () const { return false; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }
  /**
   * Determine whether this is a multiset-valued expression
   * @return	true if this is a multiset-valued expression
   */
  bool isSet () const { return true; }

  /** Equality comparison operator */
  bool operator== (const class EmptySet&) const { return true; }

  /** Ordering comparison operator */
  bool operator< (const class EmptySet&) const { return false; }

  /** Evaluate the multiset expression
   * @param valuation	variable substitutions and the global marking
   * @return		the filtered marking
   */
  class PlaceMarking* meval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation&,
			    class Transition*, bool) { return copy (); }

  /** Substitute some variables in the expression with expressions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution&) { return copy (); }

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

# ifdef EXPR_COMPILE
  /** Generate C code for evaluating the expression */
  void compile (class CExpression&,
		unsigned,
		const char*,
		const class VariableSet*) const {
    assert (false);
  }
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;
};

#endif // EMPTYSET_H_
