// Substitution (expression distribution for variables) -*- c++ -*-

#ifndef SUBSTITUTION_H_
# define SUBSTITUTION_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <map>

/** @file Substitution.h
 * Map from variables (quantifiers or function parameters) to expressions
 */

/* Copyright � 1999-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Substitution (expression distribution for variables) */
class Substitution
{
  /** Pointer comparator */
  struct vptr
  {
    bool operator () (const class VariableDefinition* v1,
		      const class VariableDefinition* v2) const {
      return v1 < v2;
    }
  };
  /** Map from variable definitions to expressions */
  typedef std::map<const class VariableDefinition*,class Expression*,vptr> Map;
  /** Iterator to the map */
  typedef Map::iterator iterator;
  /** Constant iterator to the map */
  typedef Map::const_iterator const_iterator;

public:
  /** Constructor */
  Substitution ();
private:
  /** Copy constructor */
  Substitution (const class Substitution& old);
  /** Assignment operator */
  class Substitution& operator= (const class Substitution& old);
public:
  /** Destructor */
  ~Substitution ();

  /** Get the expression of a variable
   * @param var		identifies the variable
   * @return		the expression, or NULL if there is none
   */
  const class Expression* getExpr (const class VariableDefinition& var) const {
    const_iterator i = myExprs.find (&var);
    return i == myExprs.end () ? NULL : i->second;
  }

  /** Get the expression of a variable
   * @param var		identifies the variable
   * @return		the expression, or NULL if there is none
   */
  class Expression* getExpr (const class VariableDefinition& var);

  /** Assign an expression to a variable.
   * @param var		identifies the variable
   * @param expr	expression to be assigned
   */
  void setExpr (const class VariableDefinition& var, class Expression& expr);

private:
  /** The expressions sorted by variables */
  Map myExprs;
};

#endif // SUBSTITUTION_H_
