// Maria union type expression class -*- c++ -*-

#ifdef __GNUC__
# pragma implementation
#endif // __GNUC__
#include "UnionTypeExpression.h"
#include "UnionType.h"
#include "UnionValue.h"
#include "BoolType.h"
#include "LeafValue.h"
#include "Net.h"
#include "Printer.h"

/** @file UnionTypeExpression.C
 * Union determinant
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

UnionTypeExpression::UnionTypeExpression (class Expression& expr,
					  card_t i) :
  myExpr (&expr), myIndex (i)
{
  assert (myExpr && myExpr->getType ());
  assert (myExpr->getType ()->getKind () == Type::tUnion);
  assert (myExpr->isBasic ());

  setType (Net::getBoolType ());
}

UnionTypeExpression::~UnionTypeExpression ()
{
  myExpr->destroy ();
}

class Value*
UnionTypeExpression::do_eval (const class Valuation& valuation) const
{
  class Value* v = myExpr->eval (valuation);
  if (!v)
    return NULL;

  assert (v->getKind () == Value::vUnion);
  bool is = static_cast<class UnionValue*>(v)->getIndex () == myIndex;
  delete v;

  return constrain (valuation, new class LeafValue (*getType (), is));
}

class Expression*
UnionTypeExpression::ground (const class Valuation& valuation,
			     class Transition* transition,
			     bool declare)
{
  class Expression* e = myExpr->ground (valuation, transition, declare);
  if (!e)
    return NULL;
  assert (valuation.isOK ());
  if (e == myExpr) {
    e->destroy ();
    return copy ();
  }
  else
    return static_cast<class Expression*>
      (new class UnionTypeExpression (*e, myIndex))->ground (valuation);
}

class Expression*
UnionTypeExpression::substitute (class Substitution& substitution)
{
  class Expression* e = myExpr->substitute (substitution);
  if (e == myExpr) {
    e->destroy ();
    return copy ();
  }
  else
    return (new class UnionTypeExpression (*e, myIndex))->cse ();
}

bool
UnionTypeExpression::depends (const class VariableSet& vars,
			      bool complement) const
{
  return myExpr->depends (vars, complement);
}

bool
UnionTypeExpression::forExpressions (bool (*operation)
				     (const class Expression&,void*),
				     void* data) const
{
  return
    (*operation) (*this, data) &&
    myExpr->forExpressions (operation, data);
}

#ifdef EXPR_COMPILE
# include "CExpression.h"

void
UnionTypeExpression::compile (class CExpression& cexpr,
			      unsigned indent,
			      const char* lvalue,
			      const class VariableSet* vars) const
{
  char* rvalue = 0;
  class StringBuffer& out = cexpr.getOut ();
  if (cexpr.getVariable (*myExpr, rvalue))
    myExpr->compile (cexpr, indent, rvalue, vars);
  out.indent (indent);
  out.append (lvalue);
  out.append ("=");
  out.append (rvalue);
  out.append (".t==");
  out.append (myIndex);
  out.append (";\n");
  delete[] rvalue;
  compileConstraint (cexpr, indent, lvalue);
}

#endif // EXPR_COMPILE

/** Determine whether an expression needs to be enclosed in parentheses
 * @param kind	kind of the expression
 * @return	whether parentheses are necessary
 */
inline static bool
needParentheses (enum Expression::Kind kind)
{
  switch (kind) {
  case Expression::eVariable:
  case Expression::eConstant:
  case Expression::eUndefined:
  case Expression::eStructComponent:
  case Expression::eUnionComponent:
  case Expression::eVectorIndex:
  case Expression::eNot:
  case Expression::eTypecast:
  case Expression::eUnop:
  case Expression::eBufferUnop:
  case Expression::eCardinality:
    return false;
  case Expression::eMarking:
  case Expression::eTransitionQualifier:
  case Expression::ePlaceContents:
  case Expression::eSubmarking:
  case Expression::eMapping:
  case Expression::eEmptySet:
  case Expression::eSet:
  case Expression::eTemporalBinop:
  case Expression::eTemporalUnop:
  case Expression::eBooleanBinop:
  case Expression::eUnionType:
  case Expression::eBinop:
  case Expression::eRelop:
  case Expression::eBuffer:
  case Expression::eBufferRemove:
  case Expression::eBufferWrite:
  case Expression::eBufferIndex:
    assert (false);
  case Expression::eIfThenElse:
  case Expression::eStruct:
  case Expression::eUnion:
  case Expression::eVector:
  case Expression::eStructAssign:
  case Expression::eVectorAssign:
  case Expression::eVectorShift:
    break;
  }

  return true;
}

void
UnionTypeExpression::display (const class Printer& printer) const
{
  if (::needParentheses (myExpr->getKind ())) {
    printer.delimiter ('(')++;
    myExpr->display (printer);
    --printer.delimiter (')');
  }
  else
    myExpr->display (printer);

  printer.delimiter (' ');
  printer.printRaw ("is");
  printer.delimiter (' ');
  printer.print (static_cast<const class UnionType*>(myExpr->getType ())
		 ->getComponentName (myIndex));
}
