// Maria transition qualifier expression class -*- c++ -*-

#ifndef TRANSITIONQUALIFIER_H_
# define TRANSITIONQUALIFIER_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file TransitionQualifier.h
 * Transition instance qualifier
 */

/* Copyright � 1998-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Transition instance qualifier expression */
class TransitionQualifier : public Expression
{
public:
  /** Constructor
   * @param transition	The transition whose instances are being qualified
   * @param expr	Qualifying expression for the transition instances
   */
  TransitionQualifier (const class Transition& transition,
		       class Expression& expr);
private:
  /** Copy constructor */
  TransitionQualifier (const class TransitionQualifier& old);
  /** Assignment operator */
  class TransitionQualifier& operator= (const class TransitionQualifier& old);
protected:
  /** Destructor */
  ~TransitionQualifier ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eTransitionQualifier; }

  /** Determine whether this is a basic expression */
  bool isBasic () const { return false; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }

  /** Equality comparison operator */
  bool operator== (const class TransitionQualifier& other) const {
    return myTransition == other.myTransition && *myExpr == *other.myExpr;
  }

  /** Ordering comparison operator */
  bool operator< (const class TransitionQualifier& other) const {
    if (myTransition < other.myTransition)
      return true;
    if (other.myTransition < myTransition)
      return false;
    return *myExpr < *other.myExpr;
  }

  /** Partially evaluate the expression using a valuation
   * @param valuation	Variable substitutions
   * @param transition	Transition for registering quantified variables
   * @param declare	flag: declare new variables if required
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation& valuation,
			    class Transition* transition,
			    bool declare);

  /** Substitute some variables in the expression with expressions
   * @param substitution	Variable substitutions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution& substitution);

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

  /** Remove transition qualifiers from the expression
   * @param transition	the transition for which the expression is qualified
   * @return		the expession without qualifiers, or NULL
   */
  class Expression* disqualify (const class Transition& transition);

# ifdef EXPR_COMPILE
  /** Generate C code for evaluating the expression */
  void compile (class CExpression&,
		unsigned,
		const char*,
		const class VariableSet*) const {
    assert (false);
  }
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The transition */
  const class Transition* myTransition;
  /** The qualifier expression */
  class Expression* myExpr;
};

#endif // TRANSITIONQUALIFIER_H_
