// Multi-set of expressions -*- c++ -*-

#ifndef EXPRESSIONMSET_H_
# define EXPRESSIONMSET_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"
# include <map>

/** @file ExpressionMSet.h
 * Multi-set of expressions (transient storage for quantification)
 */

/* Copyright � 2000-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Multi-set of expressions */
class ExpressionMSet
{
  /** Pointer wrapper */
  class eptr
  {
  public:
    eptr () : ptr (0) {}
    eptr (class Expression& expr) : ptr (expr.copy ()) {}
    ~eptr () { if (ptr) ptr->destroy (); }
    eptr (const class eptr& old) : ptr (old.ptr ? old.ptr->copy () : 0) {}
  private:
    class eptr& operator= (const class eptr& old);
  public:
    operator class Expression* () const { return ptr->copy (); }
    operator const class Expression* () const { return ptr; }

    bool operator== (const class eptr& other) const {
      return *ptr == *other.ptr;
    }
    bool operator< (const class eptr& other) const {
      return *ptr < *other.ptr;
    }
  private:
    class Expression* ptr;
  };

  /*@{*/
  /** Multi-set of expressions implemented as a map */
  typedef std::map<class eptr,card_t> Map;
  /** Constant iterator to the multi-set */
  typedef Map::const_iterator const_iterator;
  /** Iterator to the multi-set */
  typedef Map::iterator iterator;
  /*@}*/

public:
  /** Constructor */
  ExpressionMSet () : myMap () {}
private:
  /** Copy constructor */
  ExpressionMSet (const class ExpressionMSet& old);
  /** Assignment operator */
  class ExpressionMSet& operator= (const class ExpressionMSet& old);
public:
  /** Destructor */
  ~ExpressionMSet () {}

  /** Insert an expression to the multi-set
   * @param expr	expression to be inserted
   * @param count	multiplicity of the expression
   * @return		whether the operation was successful (no overflow)
   */
  bool insert (class Expression& expr, card_t count) {
    assert (count > 0);
    std::pair<iterator,bool> status =
      myMap.insert (Map::value_type (expr, count));
    if (!status.second) {
      if (count >= CARD_T_MAX - status.first->second)
	return false;
      else
	status.first->second += count;
    }
    return true;
  }

  /** Convert the multi-set to a marking expression
   * @param place	the place associated with the marking expressions
   * @return		a marking list
   */
  class Marking* toMarking (const class Place* place);

  /** @name Accessors to the expression multi-set */
  /*@{*/
  void clear () { myMap.clear (); }
  iterator begin () { return myMap.begin (); }
  iterator end () { return myMap.end (); }
  const_iterator begin () const { return myMap.begin (); }
  const_iterator end () const { return myMap.end (); }
  /*@}*/

private:
  /** The marking set */
  Map myMap;
};

#endif // EXPRESSIONMSET_H_
