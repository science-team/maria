// Set of expressions -*- c++ -*-

#ifndef EXPRESSIONSET_H_
# define EXPRESSIONSET_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"
# include <assert.h>
# include <set>

/** @file ExpressionSet.h
 * Set of expressions
 */

/* Copyright � 1999-2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Set of expressions */
class ExpressionSet
{
  /** Pointer wrapper */
  class eptr
  {
  public:
    eptr () : ptr (0) {}
    eptr (class Expression* expr) : ptr (expr->copy ()) {}
    ~eptr () { if (ptr) ptr->destroy (); }
    eptr (const class eptr& old) : ptr (old.ptr ? old.ptr->copy () : 0) {}
  private:
    class eptr& operator= (const class eptr& old);
  public:
    operator class Expression* () const { return ptr->copy (); }
    operator const class Expression* () const { return ptr; }

    bool operator== (const class eptr& other) const {
      return *ptr == *other.ptr;
    }
    bool operator< (const class eptr& other) const {
      return *ptr < *other.ptr;
    }
  private:
    class Expression* ptr;
  };

  /*@{*/
  /** Set of expressions */
  typedef std::set<class eptr> Set;
  /** Constant iterator to the expression set */
  typedef Set::const_iterator const_iterator;
  /** Iterator to the expression set */
  typedef Set::iterator iterator;
  /*@}*/

public:
  /** Constructor */
  ExpressionSet () : mySet () {}
private:
  /** Copy constructor */
  ExpressionSet (const class ExpressionSet& old);
  /** Assignment operator */
  class ExpressionSet& operator= (const class ExpressionSet& old);
public:
  /** Destructor */
  ~ExpressionSet () {}

  /** Determine whether the ExpressionSet is empty */
  bool empty () const { return mySet.empty (); }

  /** Insert an Expression to the set
   * @param expr	Expression to be inserted
   * @return		expr or an equivalent expression
   */
  class Expression* insert (class Expression* expr) {
    assert (expr != NULL);
    std::pair<iterator,bool> status;
    {
      eptr e (expr);
      status = mySet.insert (e);
    }
    if (!status.second) {
      assert (expr->isLastCopy () ||
	      expr == static_cast<const class Expression*>(*status.first));
      expr->destroy ();
      return *status.first;
    }
    else
      return expr;
  }

  /** @name Accessors to the expression set */
  /*@{*/
  iterator begin () { return mySet.begin (); }
  iterator end () { return mySet.end (); }
  const_iterator begin () const { return mySet.begin (); }
  const_iterator end () const { return mySet.end (); }
  /*@}*/

private:
  /** The expression set */
  Set mySet;
};

#endif // EXPRESSIONSET_H_
