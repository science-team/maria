// Multi-set mapping expression -*- c++ -*-

#ifndef MAPPING_H_
# define MAPPING_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file Mapping.h
 * Multi-set mapping operation
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Multi-set mapping */
class Mapping : public Expression
{
public:
  /** Constructor
   * @param variable	a variable that will run through marking values
   * @param cardinality	a variable that will run through marking cardinalities
   * @param marking	a multi-set expression
   * @param expr	the mapping expression for values or cardinalities
   */
  Mapping (class VariableDefinition& variable,
	   class VariableDefinition* cardinality,
	   class Expression& marking,
	   class Expression& expr);
private:
  /** Copy constructor */
  Mapping (const class Mapping& old);
  /** Assignment operator */
  class Mapping& operator= (const class Mapping& old);
protected:
  /** Destructor */
  ~Mapping ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eMapping; }

  /**
   * Determine whether this is a basic expression containing
   * no temporal logic or set operations
   * @return	true if this is a basic expression
   */
  bool isBasic () const { return false; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }
  /**
   * Determine whether this is a multiset-valued expression
   * @return	true if this is a multiset-valued expression
   */
  bool isSet () const { return true; }

  /** Equality comparison operator */
  bool operator== (const class Mapping& other) const {
    return myVariable == other.myVariable &&
      myCardinality == other.myCardinality &&
      *myMarking == *other.myMarking && *myExpr == *other.myExpr;
  }

  /** Ordering comparison operator */
  bool operator< (const class Mapping& other) const {
    if (myVariable < other.myVariable) return true;
    if (other.myVariable < myVariable) return false;
    if (myCardinality < other.myCardinality) return true;
    if (other.myCardinality < myCardinality) return false;
    if (*myMarking < *other.myMarking) return true;
    if (*other.myMarking < *myMarking) return false;
    return *myExpr < *other.myExpr;
  }

  /** Evaluate the multiset expression
   * @param valuation	variable substitutions and the global marking
   * @return		the filtered marking
   */
  class PlaceMarking* meval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @param valuation	Variable substitutions
   * @param transition	Transition for registering quantified variables
   * @param declare	flag: declare new variables if required
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation& valuation,
			    class Transition* transition,
			    bool declare);

  /** Substitute some variables in the expression with expressions
   * @param substitution	Variable substitutions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution& substitution);

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

# ifdef EXPR_COMPILE
  /** Generate C code for evaluating the expression */
  void compile (class CExpression&,
		unsigned,
		const char *,
		const class VariableSet*) const {
    assert (false);
  }

  /** Generate C code for evaluating the multi-set expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param resulttype	type of result (optional typecast qualifier)
   * @param result	multi-set to add items to
   * @param vars	the variables that have been assigned a value
   * @return		the compiled multi-set expression
   */
  void compileMset (class CExpression& cexpr,
		    unsigned indent,
		    const char* resulttype,
		    const char* result,
		    const class VariableSet* vars) const;
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The value iterator variable */
  class VariableDefinition* myVariable;
  /** The cardinality iterator variable */
  class VariableDefinition* myCardinality;
  /** The marking expression */
  class Expression* myMarking;
  /** The mapping expression */
  class Expression* myExpr;
};

#endif // MAPPING_H_
