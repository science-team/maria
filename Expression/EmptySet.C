// Empty set expression -*- c++ -*-

#ifdef __GNUC__
# pragma implementation
#endif // __GNUC__
#include "EmptySet.h"
#include "PlaceMarking.h"
#include "Printer.h"

/** @file EmptySet.C
 * Literal for empty multi-sets
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

class PlaceMarking*
EmptySet::meval (const class Valuation&) const
{
  return new class PlaceMarking;
}

bool
EmptySet::depends (const class VariableSet&,
		   bool) const
{
  return false;
}

bool
EmptySet::forExpressions (bool (*operation)
			  (const class Expression&,void*),
			  void* data) const
{
  return
    (*operation) (*this, data);
}

void
EmptySet::display (const class Printer& printer) const
{
  printer.printRaw ("empty");
}
