// Multi-set filter -*- c++ -*-

#ifndef SUBMARKING_H_
# define SUBMARKING_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file Submarking.h
 * Multi-set filter
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Multi-set filter */
class Submarking : public Expression
{
public:
  /** Constructor
   * @param variable	a variable that will run through marking
   * @param marking	a multi-set expression
   * @param condition	the criterion for filtering the multiset items
   */
  Submarking (class VariableDefinition& variable,
	      class Expression& marking, class Expression& condition);
private:
  /** Copy constructor */
  Submarking (const class Submarking& old);
  /** Assignment operator */
  class Submarking& operator= (const class Submarking& old);
protected:
  /** Destructor */
  ~Submarking ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eSubmarking; }

  /**
   * Determine whether this is a basic expression containing
   * no temporal logic or set operations
   * @return	true if this is a basic expression
   */
  bool isBasic () const { return false; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }
  /**
   * Determine whether this is a multiset-valued expression
   * @return	true if this is a multiset-valued expression
   */
  bool isSet () const { return true; }

  /** Equality comparison operator */
  bool operator== (const class Submarking& other) const {
    return myVariable == other.myVariable &&
      *myMarking == *other.myMarking && *myCondition == *other.myCondition;
  }

  /** Ordering comparison operator */
  bool operator< (const class Submarking& other) const {
    if (myVariable < other.myVariable) return true;
    if (other.myVariable < myVariable) return false;
    if (*myMarking < *other.myMarking) return true;
    if (*other.myMarking < *myMarking) return false;
    return *myCondition < *other.myCondition;
  }

  /** Evaluate the multiset expression
   * @param valuation	variable substitutions and the global marking
   * @return		the filtered marking
   */
  class PlaceMarking* meval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @param valuation	Variable substitutions
   * @param transition	Transition for registering quantified variables
   * @param declare	flag: declare new variables if required
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation& valuation,
			    class Transition* transition,
			    bool declare);

  /** Substitute some variables in the expression with expressions
   * @param substitution	Variable substitutions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution& substitution);

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

# ifdef EXPR_COMPILE
  /** Generate C code for evaluating the expression */
  void compile (class CExpression&,
		unsigned,
		const char*,
		const class VariableSet*) const {
    assert (false);
  }

  /** Generate C code for evaluating the multi-set expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param resulttype	type of result (optional typecast qualifier)
   * @param result	multi-set to add items to
   * @param vars	the variables that have been assigned a value
   * @return		the compiled multi-set expression
   */
  void compileMset (class CExpression& cexpr,
		    unsigned indent,
		    const char* resulttype,
		    const char* result,
		    const class VariableSet* vars) const;
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The iterator variable */
  class VariableDefinition* myVariable;
  /** The marking expression */
  class Expression* myMarking;
  /** The submarking criterion */
  class Expression* myCondition;
};

#endif // SUBMARKING_H_
