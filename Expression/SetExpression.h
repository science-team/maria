// set expression class -*- c++ -*-

#ifndef SETEXPRESSION_H_
# define SETEXPRESSION_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file SetExpression.h
 * Basic operations on multi-sets
 */

/* Copyright � 1998-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Set expression */
class SetExpression : public Expression
{
public:
  /** Set operator */
  enum Op { sSubset, sEquals, sIntersection, sMinus, sUnion };

protected:
  /** Constructor
   * @param op		Operator
   * @param left	Left-hand expression of the operator
   * @param right	Right-hand expression of the operator
   */
  SetExpression (enum Op op,
		 class Expression& left, class Expression& right);

private:
  /** Copy constructor */
  SetExpression (const class SetExpression& old);
  /** Assignment operator */
  class SetExpression& operator= (const class SetExpression& old);
protected:
  /** Destructor */
  ~SetExpression ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eSet; }

  /** Determine whether this is a basic expression */
  bool isBasic () const { return !isSet (); }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }
  /**
   * Determine whether this is a multiset-valued expression
   * @return	true if this is a multiset-valued expression
   */
  bool isSet () const {
    switch (myOp) {
    case sSubset:
    case sEquals:
      break;
    case sIntersection:
    case sMinus:
    case sUnion:
      return true;
    }
    return false;
  }

  /** Construct a SetExpression, reducing to EmptySet or Constant if possible
   * @param op		Operator
   * @param left	Left-hand expression of the operator
   * @param right	Right-hand expression of the operator
   * @return		a corresponding expression
   */
  static class Expression* construct (enum Op op,
				      class Expression& left,
				      class Expression& right);

  /** Equality comparison operator */
  bool operator== (const class SetExpression& other) const {
    return myOp == other.myOp &&
      *myLeft == *other.myLeft &&
      *myRight == *other.myRight;
  }

  /** Ordering comparison operator */
  bool operator< (const class SetExpression& other) const {
    if (myOp < other.myOp) return true;
    if (other.myOp < myOp) return false;
    if (*myLeft < *other.myLeft) return true;
    if (*other.myLeft < *myLeft) return false;
    return *myRight < *other.myRight;
  }

  /** Evaluate the expression
   * @param valuation	Variable substitutions
   * @return		Value of the expression, or NULL in case of error
   */
  class Value* do_eval (const class Valuation& valuation) const;

  /** Evaluate the multiset expression
   * @param valuation	variable substitutions and the global marking
   * @return		the filtered marking
   */
  class PlaceMarking* meval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @param valuation	Variable substitutions
   * @param transition	Transition for registering quantified variables
   * @param declare	flag: declare new variables if required
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation& valuation,
			    class Transition* transition,
			    bool declare);

  /** Substitute some variables in the expression with expressions
   * @param substitution	Variable substitutions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution& substitution);

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

# ifdef EXPR_COMPILE
  /** Generate C code for evaluating the expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param lvalue	C expression referring to the lvalue
   * @param vars	the variables that have been assigned a value
   */
  void compile (class CExpression& cexpr,
		unsigned indent,
		const char* lvalue,
		const class VariableSet* vars) const;

  /** Generate C code for evaluating the multi-set expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param resulttype	type of result (optional typecast qualifier)
   * @param result	multi-set to add items to
   * @param vars	the variables that have been assigned a value
   * @return		the compiled multi-set expression
   */
  void compileMset (class CExpression& cexpr,
		    unsigned indent,
		    const char* resulttype,
		    const char* result,
		    const class VariableSet* vars) const;
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The operator */
  enum Op myOp;
  /** Left-hand expression */
  class Expression* myLeft;
  /** Right-hand expression */
  class Expression* myRight;
};

#endif // SETEXPRESSION_H_
