// constant class -*- c++ -*-

#ifndef CONSTANT_H_
# define CONSTANT_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file Constant.h
 * Constant literals in expressions
 */

/* Copyright � 1998-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Constant */
class Constant : public Expression
{
public:
  /** Constructor
   * @param value	Value of the constant
   */
  Constant (class Value& value);

private:
  /** Copy constructor */
  Constant (const class Constant& old);
  /** Assignment operator */
  class Constant& operator= (const class Constant& old);
protected:
  /** Destructor */
  ~Constant ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eConstant; }

  /** Determine whether this is a basic expression */
  bool isBasic () const { return true; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }

  /** Determine the constant value */
  const class Value& getValue () const { return *myValue; }

  /** Equality comparison operator */
  bool operator== (const class Constant& other) const;

  /** Ordering comparison operator */
  bool operator< (const class Constant& other) const;

  /** Evaluate the expression
   * @param valuation	Variable substitutions
   * @return		Value of the expression, or NULL in case of error
   */
  class Value* do_eval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation&,
			    class Transition*, bool) { return copy (); }

  /** Substitute some variables in the expression with expressions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution&) { return copy (); }

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

  /** Translate the expression to a list of temporal logic connectives
   * and Boolean propositions
   * @param property	the property automaton
   * @return		the translated object
   */
  class Ltl* toFormula (class Property& property);

# ifdef EXPR_COMPILE
  /** Generate compatibility check code
   * @param cexpr	the compilation
   * @param indent	level of indentation
   * @param vars	the variables that have been unified
   * @param value	C expression referring to the desired value
   */
  void compileCompatible (class CExpression& cexpr,
			  unsigned indent,
			  const class VariableSet& vars,
			  const char* value) const;

  /** Generate C code for evaluating the expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param lvalue	C expression referring to the lvalue
   * @param vars	the variables that have been assigned a value
   */
  void compile (class CExpression& cexpr,
		unsigned indent,
		const char* lvalue,
		const class VariableSet* vars) const;
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** Value of the constant */
  class Value* myValue;
};

#endif // CONSTANT_H_
