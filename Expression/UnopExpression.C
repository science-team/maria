// unary operator class -*- c++ -*-

#ifdef __GNUC__
# pragma implementation
#endif // __GNUC__
#include "UnopExpression.h"
#include "IntType.h"
#include "CardType.h"
#include "LeafValue.h"
#include "Net.h"
#include "Printer.h"

/** @file UnopExpression.C
 * Unary operators for integer and other arithmetics
 */

/* Copyright � 1998-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

UnopExpression::UnopExpression (enum Op op, class Expression& expr) :
  Expression (),
  myOp (op), myExpr (&expr)
{
  if (myOp == uNeg)
    setType (Net::getIntType ());
  else
    setType (*expr.getType ());
  assert (myOp == uSucc || myOp == uPred
	  ? getType ()->isOrdered ()
	  : (getType ()->getKind () == Type::tInt ||
	     getType ()->getKind () == Type::tCard));
  assert (myExpr->isBasic ());
}

UnopExpression::~UnopExpression ()
{
  myExpr->destroy ();
}

class Value*
UnopExpression::do_eval (const class Valuation& valuation) const
{
  class Value* v = myExpr->eval (valuation);
  if (!v)
    return NULL;
  assert (v->getKind () == Value::vLeaf || myOp == uSucc || myOp == uPred);

  switch (myOp) {
  case uNeg:
    if (myExpr->getType ()->getKind () == Type::tInt) {
      if (int_t (static_cast<class LeafValue&>(*v)) == INT_T_MIN) {
	valuation.flag (errOver, *this);
	delete v;
	return NULL;
      }
      static_cast<class LeafValue*>(v)->setValue
	(-int_t (static_cast<class LeafValue&>(*v)));
      static_cast<class LeafValue*>(v)->setType (*getType ());
      return constrain (valuation, v);
    }
    else {
      if (card_t (static_cast<class LeafValue&>(*v)) >
	  card_t (INT_T_MAX) + 1) {
	valuation.flag (errOver, *this);
	delete v;
	return NULL;
      }
      static_cast<class LeafValue*>(v)->setValue
	(-card_t (static_cast<class LeafValue&>(*v)));
      static_cast<class LeafValue*>(v)->setType (*getType ());
      return constrain (valuation, v);
    }
  case uNot:
    static_cast<class LeafValue*>(v)->setValue
      (~card_t (static_cast<class LeafValue&>(*v)));
    return constrain (valuation, v);
  case uSucc:
    v->increment ();
    return v;
  case uPred:
    v->decrement ();
    return v;
  }

  assert (false);
  delete v;
  return NULL;
}

class Expression*
UnopExpression::ground (const class Valuation& valuation,
			class Transition* transition,
			bool declare)
{
  class Expression* e = myExpr->ground (valuation, transition, declare);
  if (!e)
    return NULL;
  assert (valuation.isOK ());
  if (e == myExpr) {
    e->destroy ();
    return copy ();
  }
  else {
    class Expression* expr = new class UnopExpression (myOp, *e);
    expr->setType (*getType ());
    return expr->ground (valuation);
  }
}

class Expression*
UnopExpression::substitute (class Substitution& substitution)
{
  class Expression* e = myExpr->substitute (substitution);
  if (e == myExpr) {
    e->destroy ();
    return copy ();
  }
  else
    return (new class UnopExpression (myOp, *e))->cse ();
}

bool
UnopExpression::depends (const class VariableSet& vars,
			 bool complement) const
{
  return myExpr->depends (vars, complement);
}

bool
UnopExpression::forExpressions (bool (*operation)
				(const class Expression&,void*),
				void* data) const
{
  return
    (*operation) (*this, data) &&
    myExpr->forExpressions (operation, data);
}

#ifdef EXPR_COMPILE
# include "CExpression.h"

void
UnopExpression::compile (class CExpression& cexpr,
			 unsigned indent,
			 const char* lvalue,
			 const class VariableSet* vars) const
{
  char* rvalue;
  class StringBuffer& out = cexpr.getOut ();
  if (cexpr.getVariable (*myExpr, rvalue))
    myExpr->compile (cexpr, indent, rvalue, vars);

  switch (myOp) {
  case uNeg:
    out.indent (indent);
    out.append ("if (");
    out.append (rvalue);
    switch (getType ()->getKind ()) {
    case Type::tInt:
      out.append ("==INT_MIN");
      break;
    case Type::tCard:
      out.append (">INT_MAX");
      break;
    default:
      assert (false);
    }
    out.append (")\n");
    cexpr.compileError (indent + 2, errOver);
    out.indent (indent);
    out.append (lvalue);
    out.append ("=-");
    out.append (rvalue);
    out.append (";\n");
    break;
  case uNot:
    out.indent (indent);
    out.append (lvalue);
    out.append ("=~");
    out.append (rvalue);
    out.append (";\n");
    break;
  case uSucc:
    getType ()->compileSuccessor (out, indent, lvalue, rvalue, NULL);
    break;
  case uPred:
    getType ()->compilePredecessor (out, indent, lvalue, rvalue, NULL);
    break;
  }
  delete[] rvalue;
  if (myOp != uSucc && myOp != uPred)
    compileConstraint (cexpr, indent, lvalue);
}

#endif // EXPR_COMPILE

/** Convert an operator to a string
 * @param op	the operator to convert
 * @return	a string corresponding to the operator
 */
inline static const char*
getOpString (enum UnopExpression::Op op)
{
  switch (op) {
  case UnopExpression::uNeg:
    return "-";
  case UnopExpression::uNot:
    return "~";
  case UnopExpression::uSucc:
    return "+";
  case UnopExpression::uPred:
    return "|";
  }

  return "???";
}

/** Determine whether an expression needs to be enclosed in parentheses
 * @param kind	kind of the expression
 * @return	whether parentheses are necessary
 */
inline static bool
needParentheses (enum Expression::Kind kind)
{
  switch (kind) {
  case Expression::eVariable:
  case Expression::eConstant:
  case Expression::eUndefined:
  case Expression::eStructComponent:
  case Expression::eUnionComponent:
  case Expression::eVectorIndex:
  case Expression::eNot:
  case Expression::eTypecast:
  case Expression::eUnop:
  case Expression::eBufferUnop:
  case Expression::eCardinality:
    return false;
  case Expression::eMarking:
  case Expression::eTransitionQualifier:
  case Expression::ePlaceContents:
  case Expression::eSubmarking:
  case Expression::eMapping:
  case Expression::eEmptySet:
    assert (false);
  case Expression::eBooleanBinop:
  case Expression::eUnionType:
  case Expression::eIfThenElse:
  case Expression::eSet:
  case Expression::eTemporalBinop:
  case Expression::eTemporalUnop:
  case Expression::eStruct:
  case Expression::eUnion:
  case Expression::eVector:
  case Expression::eBinop:
  case Expression::eRelop:
  case Expression::eBuffer:
  case Expression::eBufferRemove:
  case Expression::eBufferWrite:
  case Expression::eBufferIndex:
  case Expression::eStructAssign:
  case Expression::eVectorAssign:
  case Expression::eVectorShift:
    break;
  }

  return true;
}

void
UnopExpression::display (const class Printer& printer) const
{
  printer.printRaw (::getOpString (myOp));
  if (::needParentheses (myExpr->getKind ())) {
    printer.delimiter ('(')++;
    myExpr->display (printer);
    --printer.delimiter (')');
  }
  else
    myExpr->display (printer);
}
