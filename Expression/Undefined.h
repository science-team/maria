// undefined expression class -*- c++ -*-

#ifndef UNDEFINED_H_
# define UNDEFINED_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"

/** @file Undefined.h
 * Undefined literal
 */

/* Copyright � 1998-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Undefined expression */
class Undefined : public Expression
{
public:
  /** Fatalness levels */
  enum Fatalness {
    /** issue an error message; do not fire the transition instance */
    fError,
    /** issue a fatal error message; stop the graph generation */
    fFatalError
  };

  /** Constructor
   * @param fatalness	Fatalness level
   */
  explicit Undefined (enum Fatalness fatalness);

private:
  /** Copy constructor */
  explicit Undefined (const class Undefined& old);
  /** Assignment operator */
  class Undefined& operator= (const class Undefined& old);
protected:
  /** Destructor */
  ~Undefined ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eUndefined; }

  /** Determine whether this is a basic expression */
  bool isBasic () const { return true; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }

  /** Equality comparison operator */
  bool operator== (const class Undefined& other) const {
    return myFatalness == other.myFatalness;
  }

  /** Ordering comparison operator */
  bool operator< (const class Undefined& other) const {
    return myFatalness < other.myFatalness;
  }

  /** Evaluate the expression
   * @param valuation	Variable substitutions
   * @return		Value of the expression, or NULL in case of error
   */
  class Value* do_eval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation&,
			    class Transition*, bool) { return copy (); }

  /** Substitute some variables in the expression with expressions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution&) { return copy (); }

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

# ifdef EXPR_COMPILE
  /** Generate C code for evaluating the expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param lvalue	C expression referring to the lvalue
   * @param vars	the variables that have been assigned a value
   */
  void compile (class CExpression& cexpr,
		unsigned indent,
		const char* lvalue,
		const class VariableSet* vars) const;
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

  /** Determine whether this is a fatal expression */
  bool isFatal () const { return myFatalness == fFatalError; }
private:
  /** Fatalness level */
  enum Fatalness myFatalness;
};

#endif // UNDEFINED_H_
