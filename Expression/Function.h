// Function definition -*- c++ -*-

#ifndef FUNCTION_H_
# define FUNCTION_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <assert.h>

/** @file Function.h
 * Function (typed macro) definition
 */

/* Copyright � 1999-2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Function definition */
class Function
{
public:
  /** Constructor
   * @param name	Name of the function
   * @param arity	Arity (number of function parameters)
   * @param params	Parameter list of the function
   * @param expr	Function definition
   * @param copy	Flag: is this a copied definition?
   */
  Function (char* name,
	    unsigned arity,
	    class VariableDefinition** params,
	    class Expression* expr,
	    bool copy = false);
private:
  /** Copy constructor */
  Function (const class Function& old);
  /** Assignment operator */
  class Function& operator= (const class Function& old);
public:
  /** Destructor */
  ~Function ();

  /** Determine the name of the function */
  const char* getName () const { return myName; }

  /** Determine the arity of the function */
  unsigned getArity () const { return myArity; }

  /** Get a formal parameter */
  const class VariableDefinition& operator[] (unsigned i) const {
    assert (i < myArity); return *myParams[i];
  }

  /** Get the expression */
  const class Expression& getExpr () const { return *myExpr; }

  /** Determine whether this is a copied function definition */
  bool isCopy () const { return myCopy; }

  /** Substitute the expression */
  void substitute (class Substitution& substitution);

  /** Expand the function
   * @param params	The expression list
   * @return		The expanded function, or NULL in case of error
   */
  class Expression* expand (class ExpressionList* params);

private:
  /** Name of the function */
  char* myName;
  /** Arity of the function */
  unsigned myArity;
  /** Function parameters */
  class VariableDefinition** myParams;
  /** Function definition */
  class Expression* myExpr;
  /** Flag: is this a copied function definition? */
  const bool myCopy;
};

#endif // FUNCTION_H_
