/** @file allExpressions.h
 * Header file that includes all classes derived from Expression
 */

/* Copyright � 1998-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

#include "Typecast.h"
#include "Variable.h"
#include "Constant.h"
#include "Undefined.h"
#include "StructExpression.h"
#include "StructComponent.h"
#include "StructAssign.h"
#include "UnionExpression.h"
#include "UnionComponent.h"
#include "UnionTypeExpression.h"
#include "VectorExpression.h"
#include "VectorIndex.h"
#include "VectorAssign.h"
#include "VectorShift.h"
#include "UnopExpression.h"
#include "BinopExpression.h"
#include "BufferExpression.h"
#include "BufferUnop.h"
#include "BufferRemove.h"
#include "BufferWrite.h"
#include "BufferIndex.h"
#include "IfThenElse.h"
#include "BooleanBinop.h"
#include "NotExpression.h"
#include "RelopExpression.h"
#include "SetExpression.h"
#include "TemporalUnop.h"
#include "TemporalBinop.h"
#include "CardinalityExpression.h"
#include "Marking.h"
#include "TransitionQualifier.h"
#include "PlaceContents.h"
#include "Submarking.h"
#include "Mapping.h"
#include "EmptySet.h"
