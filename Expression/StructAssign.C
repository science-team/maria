// Maria structure assignment class -*- c++ -*-

#include "snprintf.h"

#ifdef __GNUC__
# pragma implementation
#endif // __GNUC__
#include "StructAssign.h"
#include "StructType.h"
#include "StructValue.h"
#include "Printer.h"

/** @file StructAssign.C
 * Structure component assignment operation
 */

/* Copyright � 2001-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

StructAssign::StructAssign (class Expression& structure,
			    card_t i,
			    class Expression& expr) :
  Expression (),
  myStructure (structure), myIndex (i), myExpr (expr)
{
  assert (myStructure.getType () &&
	  myStructure.getType ()->getKind () == Type::tStruct);
  assert (myStructure.isBasic () && myExpr.isBasic ());
  assert (i < static_cast<const class StructType*>
	  (myStructure.getType ())->getSize ());
  setType (*structure.getType ());
}

StructAssign::~StructAssign ()
{
  myStructure.destroy ();
  myExpr.destroy ();
}

class Value*
StructAssign::do_eval (const class Valuation& valuation) const
{
  class Value* v = myStructure.eval (valuation);
  if (!v)
    return NULL;

  assert (v->getKind () == Value::vStruct);
  class Value*& w = static_cast<class StructValue&>(*v)[myIndex];
  delete w;
  if (!(w = myExpr.eval (valuation))) {
    delete v;
    return NULL;
  }
  return constrain (valuation, v);
}

class Expression*
StructAssign::ground (const class Valuation& valuation,
		      class Transition* transition,
		      bool declare)
{
  class Expression* structure =
    myStructure.ground (valuation, transition, declare);
  if (!structure)
    return NULL;
  assert (valuation.isOK ());
  class Expression* expr = myExpr.ground (valuation, transition, declare);
  if (!expr) {
    structure->destroy ();
    return NULL;
  }

  if (structure == &myStructure && expr == &myExpr) {
    structure->destroy (), expr->destroy ();
    return copy ();
  }
  else
    return static_cast<class Expression*>
      (new class StructAssign
       (*structure, myIndex, *expr))->ground (valuation);
}

class Expression*
StructAssign::substitute (class Substitution& substitution)
{
  class Expression* structure = myStructure.substitute (substitution);
  class Expression* expr = myExpr.substitute (substitution);
  if (structure == &myStructure && expr == &myExpr) {
    structure->destroy (), expr->destroy ();
    return copy ();
  }
  else
    return (new class StructAssign (*structure, myIndex, *expr))->cse ();
}

bool
StructAssign::depends (const class VariableSet& vars,
		       bool complement) const
{
  return
    myStructure.depends (vars, complement) ||
    myExpr.depends (vars, complement);
}

bool
StructAssign::forExpressions (bool (*operation)
			      (const class Expression&,void*),
			      void* data) const
{
  return
    (*operation) (*this, data) &&
    myStructure.forExpressions (operation, data) &&
    myExpr.forExpressions (operation, data);
}

#ifdef EXPR_COMPILE
# include "CExpression.h"
# include <stdio.h>
# include <string.h>

void
StructAssign::compile (class CExpression& cexpr,
		       unsigned indent,
		       const char* lvalue,
		       const class VariableSet* vars) const
{
  size_t len = strlen (lvalue);
  char* rvalue = new char[len + 23];
  memcpy (rvalue, lvalue, len);
  snprintf (rvalue + len, 23, "[%u]", myIndex);
  myStructure.compile (cexpr, indent, lvalue, vars);
  myExpr.compile (cexpr, indent, rvalue, vars);
  delete[] rvalue;
  compileConstraint (cexpr, indent, lvalue);
}

#endif // EXPR_COMPILE

void
StructAssign::display (const class Printer& printer) const
{
  myStructure.display (printer);
  printer.delimiter ('.');
  printer.delimiter ('{')++;
  printer.print (static_cast<const class StructType*>(myStructure.getType ())
		 ->getComponentName (myIndex));
  printer.delimiter (' ');
  myExpr.display (printer);
  --printer.delimiter ('}');
}
