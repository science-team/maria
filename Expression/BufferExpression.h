// Maria buffer expression class -*- c++ -*-

#ifndef BUFFEREXPRESSION_H_
# define BUFFEREXPRESSION_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Expression.h"
# include "BufferType.h"

# include <assert.h>

/** @file BufferExpression.h
 * Queue or stack constructor
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Buffer initialization expression */
class BufferExpression : public Expression
{
public:
  /** Constructor
   * @param type	Type of the expression
   */
  BufferExpression (const class BufferType& type);
private:
  /** Copy constructor */
  BufferExpression (const class BufferExpression& old);
  /** Assignment operator */
  class BufferExpression& operator= (const class BufferExpression& old);
protected:
  /** Destructor */
  ~BufferExpression ();
public:

  /** Determine the type of the expression */
  enum Expression::Kind getKind () const { return eBuffer; }

  /** Set the type of the expression
   * @param type	Type of the expression
   */
  void setType (const class Type& type);

  /** Determine whether this is a basic expression */
  bool isBasic () const { return true; }
  /** Determine whether this is a temporal logic expression */
  bool isTemporal () const { return false; }

  /** Append a component
   * @param component	The component expression to be appended
   */
  void append (class Expression& component) {
    assert (hasCapacity ());
    assert (component.isBasic ());
    myComponents[getSize ()] = &component;
  }

  /** Determine whether there is capacity left */
  bool hasCapacity () const {
    card_t size = static_cast<const class BufferType*>(getType ())->getSize ();
    return size && !myComponents[size - 1];
  }

  /** Determine the current size of the buffer expression */
  card_t getSize () const {
    for (card_t i =
	   static_cast<const class BufferType*>(getType ())->getSize ();
	 i--; ) {
      if (myComponents[i])
	return i + 1;
    }
    return 0;
  }

  /** Equality comparison operator */
  bool operator== (const class BufferExpression& other) const {
    const class BufferType* v =
      static_cast<const class BufferType*>(getType ());
    const class BufferType* u =
      static_cast<const class BufferType*>(other.getType ());
    if (v->isStack () != u->isStack () ||
	v->getSize () != u->getSize ())
      return false;
    for (card_t i = 0; i < v->getSize (); i++)
      if (!myComponents[i]) {
	if (other.myComponents[i])
	  return false;
	else
	  continue;
      }
      else if (!other.myComponents[i] ||
	       !(*(myComponents[i]) == *(other.myComponents[i])))
	return false;

    return true;
  }

  /** Ordering comparison operator */
  bool operator< (const class BufferExpression& other) const {
    const class BufferType* v =
      static_cast<const class BufferType*>(getType ());
    const class BufferType* u =
      static_cast<const class BufferType*>(other.getType ());
    if (v->getSize () < u->getSize ())
      return true;
    if (u->getSize () < v->getSize ())
      return false;
    if (!v->isStack () && u->isStack ())
      return true;
    if (!u->isStack () && v->isStack ())
      return false;
    for (card_t i = v->getSize (); i--; )
      if (!myComponents[i]) {
	if (other.myComponents[i])
	  return true;
	else
	  continue;
      }
      else if (!other.myComponents[i])
	return false;
      else if (*(myComponents[i]) < *(other.myComponents[i]))
	return true;
      else if (*(other.myComponents[i]) < *(myComponents[i]))
	return false;

    return false;
  }

  /** Evaluate the expression
   * @param valuation	Variable substitutions
   * @return		Value of the expression, or NULL in case of error
   */
  class Value* do_eval (const class Valuation& valuation) const;

  /** Partially evaluate the expression using a valuation
   * @param valuation	Variable substitutions
   * @param transition	Transition for registering quantified variables
   * @param declare	flag: declare new variables if required
   * @return		grounded expression, or NULL in case of error
   */
  class Expression* ground (const class Valuation& valuation,
			    class Transition* transition,
			    bool declare);

  /** Substitute some variables in the expression with expressions
   * @param substitution	Variable substitutions
   * @return			substituted expression
   */
  class Expression* substitute (class Substitution& substitution);

  /** Determine whether the expression depends on a set of variables
   * @param vars	the set of variables
   * @param complement	flag: treat the set as its complement
   */
  bool depends (const class VariableSet& vars,
		bool complement) const;

  /** Perform an operation on all subexpressions of the expression
   * @param operation	operation to be performed (return false on failure)
   * @param data	parameters to be passed to the operation
   * @return		true if all operations succeeded
   */
  bool forExpressions (bool (*operation)
		       (const class Expression&,void*),
		       void* data) const;

  /** Determine whether the expression is compatible with the specified value,
   * neglecting subexpressions that cannot be evaluated
   * @param value	value the expression will be compared to
   * @param valuation	variable substitutions
   * @return		true if the expression is compatible with the value
   */
  bool isCompatible (const class Value& value,
		     const class Valuation& valuation) const;

  /** Unify variables from this expression
   * @param value	the value the expression should evaluate to
   * @param valuation	variable substitutions
   * @param vars	the variables to unify
   */
  void getLvalues (const class Value& value,
		   class Valuation& valuation,
		   const class VariableSet& vars) const;

  /** Determine which variables could be unified from this expression
   * @param rvalues	variables unified so far
   * @param lvalues	(output) variables that could be unified
   * @return		variables that could be unified
   */
  void getLvalues (const class VariableSet& rvalues,
		   class VariableSet*& lvalues) const;

# ifdef EXPR_COMPILE
  /** Generate lvalue gathering code
   * @param cexpr	the compilation
   * @param indent	level of indentation
   * @param vars	the variables
   * @param lvalue	C expression referring to the value
   */
  void compileLvalue (class CExpression& cexpr,
		      unsigned indent,
		      const class VariableSet& vars,
		      const char* lvalue) const;

  /** Generate compatibility check code
   * @param cexpr	the compilation
   * @param indent	level of indentation
   * @param vars	the variables that have been unified
   * @param value	C expression referring to the desired value
   */
  void compileCompatible (class CExpression& cexpr,
			  unsigned indent,
			  const class VariableSet& vars,
			  const char* value) const;

  /** Generate C code for evaluating the expression
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param lvalue	C expression referring to the lvalue
   * @param vars	the variables that have been assigned a value
   */
  void compile (class CExpression& cexpr,
		unsigned indent,
		const char* lvalue,
		const class VariableSet* vars) const;
# endif // EXPR_COMPILE

  /** Display the expression
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The expressions for the buffer components */
  class Expression** myComponents;
};

#endif // BUFFEREXPRESSION_H_
