/** @file snprintf.h
 * Prototype for snprintf(3)
 */

/* Copyright � 2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

#if defined __GNUC__ && __GNUC__ < 3
# ifndef _GNU_SOURCE
/* snprintf was a GNU extension until the C99 standard */
#  define _GNU_SOURCE
# endif /* _GNU_SOURCE */
#endif /* __GNUC__ < 3 */
#if defined __sun
# ifndef __EXTENSIONS__
#  define __EXTENSIONS__
# endif /* __EXTENSIONS__ */
#endif /* __sun */

#ifdef __alpha
# include </usr/include/stdio.h>
#else
# include <stdio.h>
#endif

#ifndef snprintf
# if defined __linux__
# elif defined __FreeBSD__ || defined __NetBSD__ || defined __OpenBSD__
# elif defined __APPLE__
# elif defined __sgi
# elif defined __CYGWIN__
# elif defined __sun
# elif defined _AIX
#  define __EXTENSIONS__
# elif defined __WIN32 || defined __WIN32__
#  define snprintf _snprintf
# else
#  define snprintf(s,n,p,f) sprintf(s,p,f)
# endif
#endif /* !snprintf */
