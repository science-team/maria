#!/usr/bin/perl

# Convert unfolded place and transition names to more readable format
while (<>) { s/\b(TR|PL)_([A-Za-z0-9_]*)\b/&demangle($2)/ego; print; }

# Demangle an unfolded place or transition name
sub demangle ()
{
    my($name) = @_;
    my(@names) = split ("__", $name);
    map { s/_([0-9A-Fa-f]{2})/pack ("C", hex "$1")/ge } @names;

    $name = shift @names;
    if ($#names >= 0)
    {
	my($delim) = "<";
	while ($#names >= 0)
	{
	    $name .= $delim . (shift @names);
	    last unless ($#names >= 0);
	    $name .= ":" . (shift @names);
	    $delim = ","
	    }
	$name .= ">";
    }
    return $name;
}
