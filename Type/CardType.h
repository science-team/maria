// Unsigned integer (cardinality) type class -*- c++ -*-

#ifndef CARDTYPE_H_
# define CARDTYPE_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Type.h"

/** @file CardType.h
 * Unsigned integer data type
 */

/* Copyright � 2000-2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Unsigned integer (cardinality) type */
class CardType : public Type
{
public:
  /** Constructor */
  CardType () : Type (true) {}
  /** Copy constructor */
  CardType (const class CardType& old) : Type (old) {}
private:
  /** Assignment operator */
  class CardType& operator= (const class CardType& old);
public:
  /** Destructor */
  ~CardType () {}
  /** Copy the Type */
  class Type* copy () const { return new class CardType (*this); }

  /** Determine the kind of the type */
  enum Kind getKind () const { return tCard; }

  /** Get the first value of this type */
  class Value& getFirstValue () const;
  /** Get the last value of this type */
  class Value& getLastValue () const;
  /** Get the number of possible values for this type */
  card_t do_getNumValues () const;

  /** Convert a value of this type to a number
   * @param value	value to be converted
   * @return		number between 0 and getNumValues () - 1
   */
  card_t convert (const class Value& value) const;

  /** Convert a number to a value of this type
   * @param number	number between 0 and getNumValues () - 1
   * @return		the corresponding value
   */
  class Value* convert (card_t number) const;

  /** Convert a constrained unsigned integer value to a number
   * @param number	placeholder: number between 0 and getNumValues (c) - 1
   * @param value	value to be converted
   * @param c		the constraint
   * @return		true if the integer actually is constrained
   */
  static bool convert (card_t& number, card_t value,
		       const class Constraint& c);

  /** Convert a number to a value of this type
   * @param number	number between 0 and getNumValues (c) - 1
   * @param c		the constraint
   * @return		the corresponding unsigned integer value
   */
  static card_t convert (card_t number, const class Constraint& c);

# ifdef EXPR_COMPILE
  /** Generate a C type declaration
   * @param out		output stream for the declarations
   * @param indent	indentation level
   */
  void compileDefinition (class StringBuffer& out,
			  unsigned indent) const;

  /** Emit code for converting an unconstrained value to a number
   * @param out		output stream
   * @param indent	indentation level
   * @param value	value to be converted
   * @param number	number to be computed
   * @param add		flag: add to number instead of assigning
   */
  void do_compileConversion (class StringBuffer& out,
			     unsigned indent,
			     const char* value,
			     const char* number,
			     bool add) const;

  /** Emit code for converting a constrained cardinality to a number
   * @param out		output stream
   * @param indent	indentation level
   * @param c		the constraint
   * @param value	value to be converted
   * @param number	number to be computed
   */
  static void compileConv (class StringBuffer& out,
			   unsigned indent,
			   const class Constraint& c,
			   const char* value,
			   const char* number);

  /** Emit code for converting a number to a constrained cardinality
   * @param out		output stream
   * @param indent	indentation level
   * @param c		the constraint
   * @param number	number to be converted
   * @param value	value to be computed
   */
  static void compileReverseConv (class StringBuffer& out,
				  unsigned indent,
				  const class Constraint& c,
				  const char* number,
				  const char* value);
# endif // EXPR_COMPILE
};

#endif // CARDTYPE_H_
