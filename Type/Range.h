// Maria range class -*- c++ -*-

#ifndef RANGE_H_
# define RANGE_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

/** @file Range.h
 * Closed range of values, part of a Constraint
 */

/* Copyright � 1999-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Constant range */
class Range
{
public:
  /** Constructor
   * @param low		Lower bound of the range
   * @param high	Upper bound of the range
   */
  Range (class Value& low,
	 class Value& high);
  /** Copy constructor */
  Range (const class Range& old);
private:
  /** Assignment operator */
  class Range& operator= (const class Range& old);
public:
  /** Destructor */
  ~Range ();

  /** Get the lower bound */
  const class Value& getLow () const { return *myLow; }
  /** Get the higher bound */
  const class Value& getHigh () const { return *myHigh; }

  /** Set the type of the Range
   * @param type	type the limits of the range are to have
   */
  void setType (const class Type& type);

  /** Combine the two ranges if possible
   * @return		pointer to the union of both ranges, or NULL
   */
  class Range* combine (const class Range& other) const;
  /** Determine the intersection of the two ranges
   * @return		pointer to the intersection of both ranges, or NULL
   */
  class Range* cut (const class Range& other) const;

  /** Check whether the value passes the range constraint
   * @param value	Value to be checked
   * @return		whether the value passes
   */
  bool check (const class Value& value) const;

  /** Display the range
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** Lower bound */
  class Value* myLow;
  /** Higher bound */
  class Value* myHigh;
};

#endif // RANGE_H_
