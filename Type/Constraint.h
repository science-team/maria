// Type constraint class -*- c++ -*-

#ifndef CONSTRAINT_H_
# define CONSTRAINT_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <list>
# include "typedefs.h"

/** @file Constraint.h
 * Constraint that limits the domain of a data type
 */

/* Copyright � 1998-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Value constraint */
class Constraint
{
public:
  /*@{*/
  /** List of value ranges that the constraint allows */
  typedef std::list<class Range*> List;
  /** Iterator to the range list */
  typedef List::iterator iterator;
  /** Const iterator to the range list */
  typedef List::const_iterator const_iterator;
  /*@}*/

  /** Constructor */
  Constraint ();

  /** Copy constructor
   * @param old		constraint to be copied
   * @param type	type of the new constraint
   */
  Constraint (const class Constraint& old, const class Type& type);
private:
  /** Copy constructor */
  Constraint (const class Constraint& old);
  /** Assignment operator */
  class Constraint& operator= (const class Constraint& old);
public:
  /** Destructor */
  ~Constraint ();

  /** Set the type of the constraint */
  void setType (const class Type& type);

  /** Get the first value of the constraint
   * @return		the smallest value
   */
  const class Value& getFirstValue () const;
  /** Get the last value of the constraint
   * @return		the largest value
   */
  const class Value& getLastValue () const;
  /** Get the high bound of the value
   * @param value	value whose high bound is to be sought
   * @return		the high bound
   */
  const class Value& getNextHigh (const class Value& value) const;
  /** Get the smallest high bound greater than the value
   * @param value	value whose successor is to be sought
   * @return		the successor of the value, or NULL
   */
  const class Value* getNextLow (const class Value& value) const;
  /** Get the low bound of the value
   * @param value	value whose high bound is to be sought
   * @return		the high bound
   */
  const class Value& getPrevLow (const class Value& value) const;
  /** Get the biggest high bound smaller than the value
   * @param value	value whose predecessor is to be sought
   * @return		the predecessor of the value, or NULL
   */
  const class Value* getPrevHigh (const class Value& value) const;

  /** @name Accessors to the range list */
  /*@{*/
  bool empty () const { return myList.empty (); }
  size_t size () const { return myList.size (); }
  const_iterator begin () const { return myList.begin (); }
  const_iterator end () const { return myList.end (); }
  iterator begin () { return myList.begin (); }
  iterator end () { return myList.end (); }
  /*@}*/

  /** Append a range to the constraint
   * @param range	Range to be appended
   */
  void append (class Range& range);

  /** Restrict the constraint by another constraint
   * @param constraint	Constraint to be restricted by
   * @param type	Type of this constraint
   */
  void restrict (const class Constraint& constraint, const class Type& type);

  /** Determine whether the constraint encloses another constraint
   * @param other	the other constraint to be investigated
   * @return		true if all values accepted by the other constraint
   *			are accepted by this constraint
   */
  bool isSuperset (const class Constraint& other) const;

  /** Determine whether the two constraints are disjoint
   * @param other	the other constraint
   * @return		true if the constraints are disjoint
   */
  bool isDisjoint (const class Constraint& other) const;

  /** Determine whether the constraint accepts a value
   * @param value	value to be checked
   * @return		true if the value passes the check
   */
  bool isConstrained (const class Value& value) const;

  /**
   * Determine the number of possible values for this constraint
   * @return            number of possible values
   */
  card_t getNumValues () const;

#ifdef EXPR_COMPILE
  /** Generate C code for checking the constraint
   * @param cexpr	the compilation
   * @param indent	indentation level
   * @param value	C expression referring to the value
   */
  void compileCheck (class CExpression& cexpr,
		     unsigned indent,
		     const char* value) const;

  /** Emit code for converting a value to a number
   * @param out		output stream
   * @param indent	indentation level
   * @param type	the data type of the constraint
   * @param value	value to be converted
   * @param number	number to be computed
   * @param add		flag: add to number instead of assigning
   * @param work	auxiliary variable (NULL for leaf types)
   */
  void compileConversion (class StringBuffer& out,
			  unsigned indent,
			  const class Type& type,
			  const char* value,
			  const char* number,
			  bool add,
			  const char* work) const;

  /** Emit code for converting a number to a value
   * @param out		output stream
   * @param indent	indentation level
   * @param type	the data type of the constraint
   * @param number	number to be converted
   * @param value	value to be computed
   * @param leaf	flag: generate leaf type comparisons and assignments
   */
  void compileReverseConversion (class StringBuffer& out,
				 unsigned indent,
				 const class Type& type,
				 const char* number,
				 const char* value,
				 bool leaf) const;
#endif // EXPR_COMPILE

  /** Display the constraint definition
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** List of ranges belonging to the constraint */
  List myList;
};

#endif // CONSTRAINT_H_
