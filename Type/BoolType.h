// Boolean type class -*- c++ -*-

#ifndef BOOLTYPE_H_
# define BOOLTYPE_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Type.h"

/** @file BoolType.h
 * Boolean data type
 */

/* Copyright � 1998-2001,2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Boolean type */
class BoolType : public Type
{
public:
  /** Constructor */
  BoolType () : Type (true) {}
  /** Copy constructor */
  BoolType (const class BoolType& old) : Type (old) {}
private:
  /** Assignment operator */
  class BoolType& operator= (const class BoolType& old);
public:
  /** Destructor */
  ~BoolType () {}
  /** Copy the Type */
  class Type* copy () const { return new class BoolType (*this); }

  /** Determine the kind of the type */
  enum Kind getKind () const { return tBool; }

  /** Get the first value of this type */
  class Value& getFirstValue () const;
  /** Get the last value of this type */
  class Value& getLastValue () const;
  /** Get the number of possible values for this type. */
  card_t do_getNumValues () const;

  /** Convert a value of this type to a number
   * @param value	value to be converted
   * @return		number between 0 and getNumValues () - 1
   */
  card_t convert (const class Value& value) const;

  /** Convert a number to a value of this type
   * @param number	number between 0 and getNumValues () - 1
   * @return		the corresponding value
   */
  class Value* convert (card_t number) const;

# ifdef EXPR_COMPILE
  /** Generate a C type declaration
   * @param out		output stream for the declarations
   * @param indent	indentation level
   */
  void compileDefinition (class StringBuffer& out,
			  unsigned indent) const;

  /** Emit code for converting an unconstrained value to a number
   * @param out		output stream
   * @param indent	indentation level
   * @param value	value to be converted
   * @param number	number to be computed
   * @param add		flag: add to number instead of assigning
   */
  void do_compileConversion (class StringBuffer& out,
			     unsigned indent,
			     const char* value,
			     const char* number,
			     bool add) const;
# endif // EXPR_COMPILE
};

#endif // BOOLTYPE_H_
