/* Type definitions used in the Maria expression evaluator -*- c++ -*- */

#ifndef TYPEDEFS_H_
# define TYPEDEFS_H_

# include <sys/types.h>
# include <limits.h>

/** @file typedefs.h
 * Type definitions used in the expression evaluator
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Signed integer type */
typedef signed int int_t;
/** Minimum value of int_t */
# define INT_T_MIN INT_MIN
/** Maximum value of int_t */
# define INT_T_MAX INT_MAX
/** Number of bits needed for representing int_t */
# define INT_T_BIT (CHAR_BIT * sizeof (int_t))
/** Unsigned integer (cardinality) type */
typedef unsigned int card_t;
/** Maximum value of card_t */
# define CARD_T_MAX UINT_MAX
/** Number of bits needed for representing card_t */
# define CARD_T_BIT (CHAR_BIT * sizeof (card_t))
/** Character type */
typedef unsigned char char_t;
/** Maximum value of char_t */
# define CHAR_T_MAX UCHAR_MAX
/** Number of bits needed for representing char_t */
# define CHAR_T_BIT (CHAR_BIT * sizeof (char_t))

#endif // TYPEDEFS_H_
