How to use these examples
=========================

To run the examples included in this directory you need write access
to the directory. The best way is to create a copy, say as a subdirectory
of the directory <someplace>:

  cp -r /usr/share/maria/examples <someplace>
  cd <someplace>/examples
  gunzip *.gz

You can start maria on one of the Petri nets contained in this directory,
for instance dining.pn, by typing

  maria -b dining.pn

Note that this may take a *lot* of time for some of the examples
provided since a reachability analysis is performed when loading the
model.  Then, you may issue at the maria shell prompt the following
commands (among others):

  dump	     to obtain a textual representation of the syntax graph
  dumpgraph  to obtain a textual representation of the reachability graph

If the "graphviz" package is installed on your system you may also
obtain a graphical output by issuing the following commands:

 visual dump        graphical representation of the syntax graph
 visual dumpgraph   graphical representation of the reachability graph

Please refer to the maria documentation (available in the package
maria-doc) for more complete documentation.

-- Ralf Treinen <treinen@debian.org>  July 6, 2005.
