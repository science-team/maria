// Value list class -*- c++ -*-

#ifndef VALUELIST_H_
# define VALUELIST_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <assert.h>
# include "typedefs.h"

/** @file ValueList.h
 * List of values (components of StructValue, BufferValue or VectorValue)
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Value list */
class ValueList
{
public:
  /** Constructor
   * @param size	Size of the value list (number of elements)
   */
  ValueList (card_t size);
  /** Copy constructor */
  ValueList (const class ValueList& old);
private:
  /** Assignment operator */
  class ValueList& operator= (const class ValueList& old);
public:
  /** Destructor */
  ~ValueList ();

  /** Determine the size of the list */
  card_t getSize () const { return mySize; }
  /** Address an element by index */
  class Value*& operator[] (card_t i) {
    assert (i < mySize);
    return myComponents[i];
  }
  /** Address an element by index */
  const class Value* operator[] (card_t i) const {
    assert (i < mySize);
    return myComponents[i];
  }

private:
  /** Size of the list */
  card_t mySize;
  /** The values */
  class Value** myComponents;
};

#endif // VALUELIST_H_
