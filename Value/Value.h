// Value base class -*- c++ -*-

#ifndef VALUE_H_
# define VALUE_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__
# include <assert.h>
# include "typedefs.h"

/** @file Value.h
 * Abstract base class for values
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Value base class */
class Value
{
public:
  /** Value kinds (@see getKind) */
  enum Kind {
    vLeaf, vStruct, vUnion, vVector, vBuffer
  };

  /** Constructor
   * @param type	Type of the Value
   */
  Value (const class Type& type) : myType (&type) {}
  /** Copy constructor */
  Value (const class Value& old) : myType (old.myType) {}
private:
  /** Assignment operator */
  class Value& operator= (const class Value& old);
public:
  /** Destructor */
  virtual ~Value () {}
  /** Virtual copy constructor */
  virtual class Value* copy () const = 0;

  /** Determine the kind of the value */
  virtual enum Kind getKind () const = 0;
  /** Determine the type of the value */
  const class Type& getType () const { return *myType; }
  /** Set the type of the value */
  void setType (const class Type& type) { myType = &type; }

  /** Less-than comparison */
  bool operator< (const class Value& other) const;
  /** Equality comparison */
  bool operator== (const class Value& other) const;

  /** Difference operator
   * @param other	value to be substracted from this (may not be less)
   * @return		the difference
   */
  card_t operator- (const class Value& other) const;

  /** Reset the value to the first value */
  virtual void bottom () = 0;
  /** Reset the value to the last value */
  virtual void top () = 0;
  /** Get the next value
   * @return	false if the value wrapped around; otherwise true
   */
  virtual bool increment () = 0;
  /** Get the previous value
   * @return	false if the value wrapped around; otherwise true
   */
  virtual bool decrement () = 0;

  /** Convert the value to another type if possible
   * @param type	type to be casted to
   * @return		the converted value, or NULL
   */
  virtual class Value* cast (const class Type& type);

  /** Display this object
   * @param printer	The printer object
   */
  virtual void display (const class Printer& printer) const = 0;

# ifdef EXPR_COMPILE
  /** Dump this value in C notation
   * @param out		the output stream
   */
  virtual void compile (class StringBuffer& out) const = 0;

  /** Generate C assignment statements for initializing this value
   * @param name	name of the lvalue
   * @param indent	indentation level (0=generate a compound statement)
   * @param out		output stream for the generated code
   */
  virtual void compileInit (const char* name,
			    unsigned indent,
			    class StringBuffer& out) const = 0;

  /** Generate equality or inequality comparison expression
   * @param out		output stream
   * @param indent	indentation level
   * @param var		C expression to be compared
   * @param equal	type of comparison: true=equality, false=inequality
   * @param first	flag: first component (no indentation)
   * @param last	flag: last component (no expression chaining)
   * @return		true if any code was generated
   */
  virtual bool compileEqual (class StringBuffer& out,
			     unsigned indent,
			     const char* var,
			     bool equal,
			     bool first,
			     bool last) const = 0;

  /** Generate ordering comparison expression
   * @param out		output stream
   * @param indent	indentation level
   * @param var		C expression to be compared
   * @param less	comparison type: true=less, false=greater
   * @param equal	comparison type: true=equal, false=unequal
   * @param first	flag: first component (no indentation)
   * @param last	flag: last component (no expression chaining)
   * @return		number of parentheses left open
   */
  virtual unsigned compileOrder (class StringBuffer& out,
				 unsigned indent,
				 const char* var,
				 bool less,
				 bool equal,
				 bool first,
				 bool last) const = 0;
# endif // EXPR_COMPILE

private:
  /** Type of the Value */
  const class Type* myType;
};

/** Less-or-equal comparison of values */
inline static bool
operator<= (const class Value& left,
	    const class Value& right)
{
  return !(right < left);
}

#endif // VALUE_H_
