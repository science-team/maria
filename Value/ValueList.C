// Value list class -*- c++ -*-

#ifdef __GNUC__
# pragma implementation
#endif // __GNUC__
#include "ValueList.h"
#include "Value.h"

#include <string.h>

/** @file ValueList.C
 * List of values (components of StructValue, BufferValue or VectorValue)
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

ValueList::ValueList (card_t size) :
  mySize (size), myComponents (NULL)
{
  if (mySize) {
    myComponents = new class Value*[mySize];
    memset (myComponents, 0, mySize * sizeof *myComponents);
  }
}

ValueList::ValueList (const class ValueList& old) :
  mySize (old.mySize), myComponents (NULL)
{
  if (mySize) {
    myComponents = new class Value*[mySize];
    for (card_t i = 0; i < mySize; i++)
      myComponents[i] =
	old.myComponents[i] ? old.myComponents[i]->copy () : NULL;
  }
}

ValueList::~ValueList ()
{
  for (card_t i = 0; i < mySize; i++)
    delete myComponents[i];

  delete[] myComponents;
}
