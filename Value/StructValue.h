// Structure value class -*- c++ -*-

#ifndef STRUCTVALUE_H_
# define STRUCTVALUE_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "Value.h"
# include "ValueList.h"

/** @file StructValue.h
 * Structure value
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Structure value */
class StructValue : public Value
{
public:
  /** Constructor
   * @param type	type of the value
   */
  StructValue (const class Type& type);
private:
  /** Copy constructor */
  StructValue (const class StructValue& old);
  /** Assignment operator */
  class StructValue& operator= (const class StructValue& old);
public:
  /** Destructor */
  ~StructValue ();
  /** Virtual copy constructor */
  class Value* copy () const { return new class StructValue (*this); }

  /** Determine the kind of the value */
  enum Kind getKind () const { return vStruct; }

  /** Determine the number of components in the struct */
  card_t getSize () const { return myComponents.getSize (); }
  /** Address a component by index */
  const class Value& operator[] (card_t i) const {
    return *myComponents[i];
  }
  /** Address a component by index */
  class Value*& operator[] (card_t i) {
    return myComponents[i];
  }

  /** Less-than comparison */
  bool operator< (const class StructValue& other) const;
  /** Equality comparison */
  bool operator== (const class StructValue& other) const;

  /** Difference operator
   * @param other	value to be substracted from this (may not be less)
   * @return		the difference
   */
  card_t operator- (const class StructValue& other) const;

  /** Reset the value to the first value */
  void bottom ();
  /** Reset the value to the last value */
  void top ();
  /** Get the next value
   * @return	false if the value wrapped around; otherwise true
   */
  bool increment ();
  /** Get the previous value
   * @return	false if the value wrapped around; otherwise true
   */
  bool decrement ();

  /** Convert the value to another type if possible
   * @param type	type to be casted to
   * @return		the converted value, or NULL
   */
  class Value* cast (const class Type& type);

  /** Display this object
   * @param printer	The printer object
   */
  void display (const class Printer& printer) const;

# ifdef EXPR_COMPILE
  /** Dump this value in C notation
   * @param out		the output stream
   */
  void compile (class StringBuffer& out) const;

  /** Generate C assignment statements for initializing this value
   * @param name	name of the lvalue
   * @param indent	indentation level (0=generate a compound statement)
   * @param out		output stream for the generated code
   */
  void compileInit (const char* name,
		    unsigned indent,
		    class StringBuffer& out) const;

  /** Generate equality or inequality comparison expression
   * @param out		output stream
   * @param indent	indentation level
   * @param var		C expression to be compared
   * @param equal	type of comparison: true=equality, false=inequality
   * @param first	flag: first component (no indenting)
   * @param last	flag: last component (no expression chaining)
   * @return		true if any code was generated
   */
  bool compileEqual (class StringBuffer& out,
		     unsigned indent,
		     const char* var,
		     bool equal,
		     bool first,
		     bool last) const;

  /** Generate ordering comparison expression
   * @param out		output stream
   * @param indent	indentation level
   * @param var		C expression to be compared
   * @param less	comparison type: true=less, false=greater
   * @param equal	comparison type: true=equal, false=unequal
   * @param first	flag: first component (no indentation)
   * @param last	flag: last component (no expression chaining)
   * @return		number of parentheses left open
   */
  unsigned compileOrder (class StringBuffer& out,
			 unsigned indent,
			 const char* var,
			 bool less,
			 bool equal,
			 bool first,
			 bool last) const;
# endif // EXPR_COMPILE

private:
  /** The components */
  class ValueList myComponents;
};

#endif // STRUCTVALUE_H_
