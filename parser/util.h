// Generic utility functions -*- c++ -*-

#ifndef UTIL_H_
# define UTIL_H_

# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "typedefs.h"

/** @file util.h
 * Generic utility functions
 */

/* Copyright � 1999-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** String comparator */
struct ltstr
{
  /** Compare two NUL-terminated character strings
   * @param s1	first string to compare
   * @param s2	second string to compare
   * @return	true if s1 sorts before s2
   */
  bool operator() (const char* s1, const char* s2) const;
};

/** Allocate a new string (like strdup(3), but using the C++ operator new) */
char*
newString (const char *s);

/** Compute logarithm of base 16, rounded up
 * @param num		number whose logarithm is to be computed
 * @return		16-based logarithm of num
 */
unsigned
log16 (card_t num);

/** Compute logarithm of base 8, rounded up
 * @param num		number whose logarithm is to be computed
 * @return		8-based logarithm of num
 */
unsigned
log8 (card_t num);

/** Compute logarithm of base 2, rounded up
 * @param num		number whose logarithm is to be computed
 * @return		number of bits required to represent num
 */
unsigned
log2 (card_t num);

#endif /* UTIL_H_ */
