// Visualization interface to Graphviz dotty(1) -*- c++ -*-

#ifndef DOTTY_H_
# define DOTTY_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "typedefs.h"

/** @file Dotty.h
 * Visualization interface to Graphviz dotty(1)
 */

/* Copyright � 2001-2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Graphviz dotty(1) interface */
class Dotty
{
protected:
  /** Constructor */
  Dotty ();
private:
  /** Copy constructor */
  Dotty (const class Dotty& old);
  /** Assignment operator */
  class Dotty& operator= (const class Dotty& old);
public:
  /** Destructor */
  ~Dotty ();

  /** Set the name of the visualization program */
  static void useVisual (const char* name);
  /** Start the visualization process
   * @return		pointer to the visualization object; NULL on failure
   */
  static class Dotty* startVisual ();
  /** Stop the visualization process */
  static void stopVisual ();
private:
  /** Clear the visualization variables */
  void clearVisual ();
public:

  /** Dump the syntax tree of the model
   * @param net		the model being dumped
   * @param adding	flag: add to existing graph
   */
  void dump (const class Net& net,
	     bool adding) const;

  /** Display a state
   * @param reporter	the reachability graph interface
   * @param state	number of the state to be displayed
   * @param adding	flag: add to existing graph
   */
  void show (const class GraphReporter& reporter,
	     card_t state, bool adding) const;

  /** Display successor arcs
   * @param reporter	the reachability graph interface
   * @param state	number of the originating state
   * @param seq		flag: fully expand deterministic sequences
   * @param adding	flag: add to existing graph
   */
  void succ (class GraphReporter& reporter,
	     card_t state, bool seq, bool adding) const;

  /** Display predecessor arcs
   * @param reporter	the reachability graph interface
   * @param state	number of the target state
   * @param seq		flag: fully expand deterministic sequences
   * @param adding	flag: add to existing graph
   */
  void pred (const class GraphReporter& reporter,
	     card_t state, bool seq, bool adding) const;

  /** Display a graph of strongly connected components
   * @param cgraph	the graph of strongly connected components
   * @param adding	flag: add to existing graph
   */
  void components (const class ComponentGraph& cgraph,
		   bool adding) const;

  /** Display a strongly connected component
   * @param reporter	the reachability graph interface
   * @param comp	number of the component
   * @param cond	condition for picking the states in the component
   * @param adding	flag: add to existing graph
   */
  void component (const class GraphReporter& reporter,
		  card_t comp,
		  const class Expression* cond,
		  bool adding) const;

  /** Dump the reachability graph of the model
   * @param reporter	the reachability graph interface
   * @param adding	flag: add to existing graph
   */
  void dumpgraph (const class GraphReporter& reporter,
		  bool adding) const;

  /** Display the succeeding components of a strongly connected component
   * @param cgraph	the graph of strongly connected components
   * @param comp	number of the component
   * @param adding	flag: add to existing graph
   */
  void comp_succ (const class ComponentGraph& cgraph,
		  card_t comp, bool adding) const;

  /** Display the preceding components of a strongly connected component
   * @param cgraph	the graph of strongly connected components
   * @param comp	number of the component
   * @param adding	flag: add to existing graph
   */
  void comp_pred (const class ComponentGraph& cgraph,
		  card_t comp, bool adding) const;

  /** Display a path in a reachability graph
   * @param reporter	the reachability graph interface
   * @param path	state numbers on the path
   * @param adding	flag: add to existing graph
   * @param reverse	flag: reverse the arcs
   */
  void show (const class GraphReporter& reporter,
	     const card_t* path,
	     bool adding,
	     bool reverse) const;

  /** @name The lower-level interface for displaying counterexample paths */
  /*@{*/
  /** Display the graph prologue
   * @param adding	flag: add to existing graph
   */
  void displayPrologue (bool adding) const;
  /** Display the graph epilogue */
  void displayEpilogue () const;
  /** Display a node in a counterexample path
   * @param m		the marking to be displayed
   */
  void displayMarking (const class GlobalMarking& m) const;
  /** Display an edge in a counterexample path
   * @param transition	the transition
   * @param valuation	the valuation
   * @param m		the destination node
   */
  void displayEdge (const class Transition& transition,
		    const class Valuation& valuation,
		    const class GlobalMarking& m) const;
  /*@}*/

private:
  /** The singleton object */
  static class Dotty theDotty;
  /** The output stream */
  class Printer* myOut;
};

#endif // DOTTY_H_
