/** List of names -*- c++ -*- */

#ifndef NAMELIST_H_
# define NAMELIST_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <list>

/** @file NameList.h
 * List of names
 */

/* Copyright � 1999-2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** List of names */
class NameList
{
public:
  /** List of names, or null-terminated character strings */
  typedef std::list<char*> List;
  /** Iterator to the list of names */
  typedef List::iterator iterator;
  /** Constant iterator to the list of names */
  typedef List::const_iterator const_iterator;

  /** Constructor */
  NameList () : myList () {}
private:
  /** Copy constructor */
  NameList (const class NameList& old);
  /** Assignment operator */
  class NameList& operator= (const class NameList& old);
public:
  /** Destructor */
  ~NameList ();

  /** Append a name to the list
   * @param name	name to be appended
   */
  void append (char* name) { myList.push_back (name); }

  /** Remove a name from the list
   * @param i		iterator to the name
   */
  void remove (iterator i) { delete[] *i; myList.erase (i); }

  /** @name Accessors to the list of names */
  /*@{*/
  bool empty () const { return myList.empty (); }
  size_t size () const { return myList.size (); }
  iterator begin () { return myList.begin (); }
  iterator end () { return myList.end (); }
  const_iterator begin () const { return myList.begin (); }
  const_iterator end () const { return myList.end (); }
  /*@}*/

private:
  /** The list */
  List myList;
};

#endif // NAMELIST_H_
