// Variable stack map to be used with nested scoping -*- c++ -*-

#ifdef __GNUC__
# pragma implementation
#endif // __GNUC__
#include "VariableStackMap.h"
#include "VariableDefinition.h"
#include <assert.h>

/** @file VariableStackMap.C
 * Nested symbol tables for variable names
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

VariableStackMap::~VariableStackMap ()
{
  for (Stack::iterator i = myStack.begin (); i != myStack.end (); i++)
    delete *i;
}

void
VariableStackMap::push (class VariableDefinition* v)
{
  myStack.push_front (v);
  Map::iterator i = myMap.find (v->getName ());
  if (i != myMap.end ())
    (*i).second.push_front (v);
  else
    myMap[v->getName ()].push_front (v);
}

void
VariableStackMap::pop ()
{
  assert (!myStack.empty ());
  Map::iterator i = myMap.find ((*myStack.begin ())->getName ());
  assert (i != myMap.end ());
  Stack& s = i->second;
  s.erase (s.begin ());
  myStack.erase (myStack.begin ());
  if (s.empty ())
    myMap.erase (i);
}

void
VariableStackMap::clear ()
{
  for (Stack::const_iterator i = myStack.begin (); i != myStack.end (); i++)
    delete *i;
  myMap.clear ();
  myStack.clear ();
}

class VariableDefinition*
VariableStackMap::find (const char* name) const
{
  Map::const_iterator i = myMap.find (name);
  if (i == myMap.end ())
    return NULL;

  const Stack& s = (*i).second;
  if (s.empty ())
    return NULL;

  return *s.begin ();
}
