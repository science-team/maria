/* A Bison parser, made by GNU Bison 1.875d.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     RELEASE = 258,
     UNTIL = 259,
     EQUIV = 260,
     IMPL = 261,
     OR = 262,
     XOR = 263,
     AND = 264,
     NEXT = 265,
     GLOBALLY = 266,
     FINALLY = 267,
     EQ = 268,
     NE = 269,
     LE = 270,
     GE = 271,
     ROR = 272,
     ROL = 273,
     IS = 274,
     MAP = 275,
     MAX_ = 276,
     MIN_ = 277,
     CARDINALITY = 278,
     EQUALS = 279,
     SUBSET = 280,
     UNION = 281,
     MINUS = 282,
     INTERSECT = 283,
     ATOM_ = 284,
     NUMBER = 285,
     CHARACTER = 286,
     NAME = 287,
     TRUE_ = 288,
     FALSE_ = 289,
     UNDEFINED = 290,
     FATAL = 291,
     TYPEDEF = 292,
     CONST_ = 293,
     PLACE = 294,
     TRANS = 295,
     IN_ = 296,
     OUT_ = 297,
     GATE = 298,
     REJECT_ = 299,
     DEADLOCK = 300,
     ENUM = 301,
     ID = 302,
     STRUCT = 303,
     QUEUE = 304,
     STACK = 305,
     RANGE = 306,
     EMPTY = 307,
     SFAIR = 308,
     WFAIR = 309,
     ENABLED = 310,
     HIDE = 311,
     PROP = 312,
     SUBNET = 313
   };
#endif
#define RELEASE 258
#define UNTIL 259
#define EQUIV 260
#define IMPL 261
#define OR 262
#define XOR 263
#define AND 264
#define NEXT 265
#define GLOBALLY 266
#define FINALLY 267
#define EQ 268
#define NE 269
#define LE 270
#define GE 271
#define ROR 272
#define ROL 273
#define IS 274
#define MAP 275
#define MAX_ 276
#define MIN_ 277
#define CARDINALITY 278
#define EQUALS 279
#define SUBSET 280
#define UNION 281
#define MINUS 282
#define INTERSECT 283
#define ATOM_ 284
#define NUMBER 285
#define CHARACTER 286
#define NAME 287
#define TRUE_ 288
#define FALSE_ 289
#define UNDEFINED 290
#define FATAL 291
#define TYPEDEF 292
#define CONST_ 293
#define PLACE 294
#define TRANS 295
#define IN_ 296
#define OUT_ 297
#define GATE 298
#define REJECT_ 299
#define DEADLOCK 300
#define ENUM 301
#define ID 302
#define STRUCT 303
#define QUEUE 304
#define STACK 305
#define RANGE 306
#define EMPTY 307
#define SFAIR 308
#define WFAIR 309
#define ENABLED 310
#define HIDE 311
#define PROP 312
#define SUBNET 313




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 523 "maria.y"
typedef union YYSTYPE {
  card_t i;
  bool flag;
  char_t c;
  char* s;
  class Expression* expr;
  class ExpressionList* exprList;
  class Transition* trans;
  class Place* place;
  class Marking* marking;
  class Type* type;
  class ComponentList* componentList;
  class Value* value;
  class Range* range;
  class Constraint* constraint;
} YYSTYPE;
/* Line 1285 of yacc.c.  */
#line 170 "maria.tab.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE pnlval;



