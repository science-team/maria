/* A Bison parser, made by GNU Bison 1.875d.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     RELEASE = 258,
     UNTIL = 259,
     EQUIV = 260,
     IMPL = 261,
     OR = 262,
     XOR = 263,
     AND = 264,
     NEXT = 265,
     GLOBALLY = 266,
     FINALLY = 267,
     EQ = 268,
     NE = 269,
     LE = 270,
     GE = 271,
     ROR = 272,
     ROL = 273,
     IS = 274,
     MAP = 275,
     MAX_ = 276,
     MIN_ = 277,
     CARDINALITY = 278,
     EQUALS = 279,
     SUBSET = 280,
     UNION = 281,
     MINUS = 282,
     INTERSECT = 283,
     ATOM_ = 284,
     NUMBER = 285,
     STATE = 286,
     COMP = 287,
     CHARACTER = 288,
     NAME = 289,
     TRUE_ = 290,
     FALSE_ = 291,
     UNDEFINED = 292,
     FATAL = 293,
     EMPTY = 294,
     PLACE = 295,
     TRANS = 296,
     IN_ = 297,
     OUT_ = 298,
     GATE = 299,
     MODEL = 300,
     GRAPH = 301,
     LSTS = 302,
     UNFOLD = 303,
     BREADTH = 304,
     DEPTH = 305,
     STRONG = 306,
     PATH = 307,
     TERMINAL = 308,
     COMPONENTS = 309,
     CD = 310,
     TRANSLATOR = 311,
     COMPILEDIR = 312,
     VISUAL = 313,
     DUMP = 314,
     DUMPGRAPH = 315,
     EVAL = 316,
     HIDE = 317,
     SUBNET = 318,
     SHOW = 319,
     SUCC = 320,
     PRED = 321,
     GO = 322,
     STATS = 323,
     TIME = 324,
     HELP = 325,
     LOG = 326,
     FUNCTION = 327,
     PROMPT = 328,
     EXIT = 329
   };
#endif
#define RELEASE 258
#define UNTIL 259
#define EQUIV 260
#define IMPL 261
#define OR 262
#define XOR 263
#define AND 264
#define NEXT 265
#define GLOBALLY 266
#define FINALLY 267
#define EQ 268
#define NE 269
#define LE 270
#define GE 271
#define ROR 272
#define ROL 273
#define IS 274
#define MAP 275
#define MAX_ 276
#define MIN_ 277
#define CARDINALITY 278
#define EQUALS 279
#define SUBSET 280
#define UNION 281
#define MINUS 282
#define INTERSECT 283
#define ATOM_ 284
#define NUMBER 285
#define STATE 286
#define COMP 287
#define CHARACTER 288
#define NAME 289
#define TRUE_ 290
#define FALSE_ 291
#define UNDEFINED 292
#define FATAL 293
#define EMPTY 294
#define PLACE 295
#define TRANS 296
#define IN_ 297
#define OUT_ 298
#define GATE 299
#define MODEL 300
#define GRAPH 301
#define LSTS 302
#define UNFOLD 303
#define BREADTH 304
#define DEPTH 305
#define STRONG 306
#define PATH 307
#define TERMINAL 308
#define COMPONENTS 309
#define CD 310
#define TRANSLATOR 311
#define COMPILEDIR 312
#define VISUAL 313
#define DUMP 314
#define DUMPGRAPH 315
#define EVAL 316
#define HIDE 317
#define SUBNET 318
#define SHOW 319
#define SUCC 320
#define PRED 321
#define GO 322
#define STATS 323
#define TIME 324
#define HELP 325
#define LOG 326
#define FUNCTION 327
#define PROMPT 328
#define EXIT 329




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 698 "marde.y"
typedef union YYSTYPE {
  card_t i;
  card_t* ia;
  bool flag;
  char_t c;
  char* s;
  class Expression* expr;
  class ExpressionList* exprList;
  const class Place* place;
  class Marking* marking;
  const class Type* type;
} YYSTYPE;
/* Line 1285 of yacc.c.  */
#line 198 "marde.tab.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE delval;



