/* Command-line interface for lexically C-like languages -*- c -*- */

#ifndef CMDLINE_H_
# define CMDLINE_H_

/** @file cmdline.h
 * Command-line interface for lexically C-like languages
 */

/* Copyright � 1999,2001 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

# ifdef __cplusplus
extern "C" {
# endif /* __cplusplus */

# ifdef HAS_READLINE
  /** Initialize the command line interface
   * @param name		name of the program
   * @param attempted_completer	the custom completion function
   * @param entry_completer	entry completion function
   */
  void init_cmdline (const char* progname,
		     char** (*attempted_completer) (const char*, int, int),
		     char* (*entry_completer) (const char*, int));
# endif /* HAS_READLINE */

  /** Read a string, possibly split over multiple lines
   * @param prompt	Prompt to be displayed to the user
   * @param prompt2	Prompt for continuation lines
   * @return		pointer to the string; NULL on EOF
   */
  const char* cmdline (const char* prompt, const char* prompt2);

# ifdef __cplusplus
}
# endif /* __cplusplus */

#endif /* CMDLINE_H_ */
