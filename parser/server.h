// Parallelized safety property analysis -*- c++ -*-

/**
 * @file server.h
 * Parallelized safety property analysis
 */

/* Copyright � 2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

#ifndef SERVER_H_
# define SERVER_H_

/** serve the clients
 * @param s		the master socket file descriptor
 * @param reporter	the state space interface
 * @param breadth	flag: apply breadth-first search
 */
void
serve (int s,
       class StateSetReporter& reporter,
       bool breadth);

/** fork client processes
 * @param num		number of clients to fork
 * @param server	(output) flag: is this the server process?
 * @return		socket number, or <0 on error
 */
int
createJobs (unsigned num,
	    bool& server);

/** Resolve a TCP/IP port/address string.
 * @param address	the address to be resolved
 * @return		pointer to the corresponding structure, or 0 on error
 */
struct sockaddr_in*
resolve (const char* address);

/** Connect to the server process specified by resolve ().
 * @return		socket number for client; 0 for the server; <0 on error
 */
int
client_connect (void);

/** Clean up the communications structures */
void
comm_cleanup (void);

#endif // SERVER_H_
