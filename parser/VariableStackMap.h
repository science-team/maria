// Variable stack map to be used with nested scoping -*- c++ -*-

#ifndef VARIABLESTACKMAP_H_
# define VARIABLESTACKMAP_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <map>
# include <list>
# include "util.h"

/** @file VariableStackMap.h
 * Nested symbol tables for variable names
 */

/* Copyright � 1999-2002,2005 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Variable stack map to be used with nested scoping */
class VariableStackMap
{
  /** Stack of variable definitions */
  typedef std::list<class VariableDefinition*> Stack;
  /** Mapping from variable names to definition stacks */
  typedef std::map<const char*,Stack,struct ltstr> Map;

public:
  /** Constructor */
  VariableStackMap () : myMap (), myStack () {}
private:
  /** Copy constructor */
  VariableStackMap (const class VariableStackMap& old);
  /** Assignment operator */
  class VariableStackMap& operator= (const class VariableStackMap& old);
public:
  /** Destructor */
  ~VariableStackMap ();

  /** Push a variable definition on the stack
   * @param v		variable definition to be pushed on the stack
   */
  void push (class VariableDefinition* v);
  /** Get the topmost quantifier from the stack */
  class VariableDefinition* top () const { return *myStack.begin (); }
  /** Pop a quantifier from the stack */
  void pop ();
  /** Determine whether the container is empty */
  bool empty () const { return myStack.empty (); }
  /** Clear the container */
  void clear ();
  /** Find the topmost variable carrying the specified variable name
   * @param name	name being sought
   */
  class VariableDefinition* find (const char* name) const;

private:
  /** The mapping from names to variable definition stacks */
  Map myMap;
  /** Stack of all variable definitions */
  Stack myStack;
};

#endif // VARIABLESTACKMAP_H_
