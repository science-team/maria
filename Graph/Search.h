// State of reachability analysis -*- c++ -*-

#ifndef SEARCH_H_
# define SEARCH_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include <list>
# include "typedefs.h"

/** @file Search.h
 * List of search states
 */

/* Copyright � 1999-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** List of search states */
class SearchList
{
public:
  /** List of the state numbers in the search stack or queue */
  typedef std::list<card_t> List;
  /** Iterator to the state number list */
  typedef List::iterator iterator;
  /** Constant iterator to the state number list */
  typedef List::const_iterator const_iterator;

  /** Constructor */
  SearchList () : myList () {}
  /** Copy constructor */
  SearchList (const class SearchList& old) : myList (old.myList) {}
  /** Assignment operator */
  class SearchList& operator= (const class SearchList& old) {
    myList = old.myList;
    return *this;
  }
  /** Destructor */
  ~SearchList () {}

  /** @name Accessors to the state number list */
  /*@{*/
  bool empty () const { return myList.empty (); }
  size_t size () const { return myList.size (); }
  const_iterator begin () const { return myList.begin (); }
  const_iterator end () const { return myList.end (); }
  void clear () { myList.clear (); }
  /*@}*/

  /** Insert an item to the list */
  void push (card_t item) { myList.push_front (item); }
  /** Fetch an item
   * @param tail	flag: retrieve from tail of list instead of head */
  card_t pop (bool tail = false) {
    iterator i;
    if (tail)
      (i = myList.end ())--;
    else
      i = myList.begin ();
    card_t search = *i;
    myList.erase (i);
    return search;
  }

private:
  /** The search list */
  List myList;
};

#endif // SEARCH_H_
