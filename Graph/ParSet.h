// Parallelized, lossless reachability set storage -*- c++ -*-

#ifndef PARSET_H_
# define PARSET_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "StateSet.h"
# include "ByteBuffer.h"
# include "file.h"

/** @file ParSet.h
 * Transient, parallelized lossless reachability set storage
 */

/* Copyright � 2002-2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Parallelized reachability set storage */
class ParSet : public StateSet
{
public:
  /** Constructor
   * @param s		the communications socket
   */
  ParSet (int s);
private:
  /** Copy constructor */
  ParSet (const class ParSet& old);
  /** Assignment operator */
  class ParSet& operator= (const class ParSet& old);
public:
  /** Destructor */
  ~ParSet ();

  /** Display path to a rejected state
   * @param dstate	the encoded rejected state (0=deadlock)
   * @param dlen	length of dstate in bytes
   * @param reporter	the state reporter
   * @param reason	reason for rejecting the state
   * @param reduced	flag: is this a reduced state space?
   */
  void reject (const void* dstate,
	       size_t dlen,
	       const class StateSetReporter& reporter,
	       enum Code reason,
	       bool reduced) const;

private:
  /** Add a state to the graph
   * @param state	the encoded state
   * @param size	length of the encoded state, in bytes
   * @return		true if the state was enqueued to mySearch
   */
  bool do_add (const void* state,
	       size_t size);

public:
  /** Fetch an encoded state
   * @param tail	flag: retrieve from tail of list instead of head
   * @param size	(output) length of the encoded stream
   * @return		the encoded state
   */
  word_t* pop (bool tail, size_t& size);

private:
  /** The socket file descriptor */
  int myFD;
  /** The reception buffer */
  mutable class BytePacker myRecv;
  /** The transmission buffer */
  mutable class BytePacker mySend;
  /** Index to the first unread byte in myRecv */
  mutable unsigned myRecvCount;
  /** Index to the first unsent byte in mySend */
  mutable unsigned mySendCount;
};

#endif // PARSET_H_
