/* Efficient file abstraction layer -*- c++ -*- */

#ifndef FILE_H_
# define FILE_H_

/** @file file.h
 * Efficient file abstraction layer.
 * Use mmap(2) and friends on Unix systems when USE_MMAP is defined.
 * As a portable fall-back method, use <cstdio> FILE pointers.
 */

/* Copyright � 2001-2002 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

# include <stdio.h>

# ifndef unix
#  if defined __unix||defined __unix__||defined __NetBSD__||defined __APPLE__
#   define unix
#  endif
# endif

# if defined unix && defined USE_MMAP
#  include <unistd.h>
#  ifdef NO_MMAP
#   include <stdlib.h>
#  else // NO_MMAP
#   include <sys/mman.h>
#  endif // NO_MMAP
/** File handle */
typedef struct
{
#  ifndef NO_MMAP
  int fd;	/**< UNIX file descriptor */
#  endif // !NO_MMAP
  long len;	/**< used file length in bytes */
  long alloc;	/**< allocated file length in bytes */
#  ifdef __sun
  caddr_t addr;	/**< address of the mapped memory area */
#  else // __sun
  void* addr;	/**< address of the mapped memory area */
#  endif // __sun
} file_t;
# else // __unix && USE_MMAP
#  undef USE_MMAP
/** File handle */
typedef FILE* file_t;
# endif // __unix && USE_MMAP

#endif // FILE_H_
