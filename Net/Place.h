// Place class -*- c++ -*-

#ifndef PLACE_H_
# define PLACE_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

# include "typedefs.h"

/** @file Place.h
 * Place in an algebraic system net
 */

/* Copyright � 1998-2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Place class */
class Place
{
protected:
  /** Constructor
   * @param net		The net containing the place
   * @param i		Index number of the place (ask Net)
   * @param name	Name of the place
   * @param capacity	Capacity constraint
   * @param type	Type of the place
   */
  Place (const class Net& net,
	 unsigned i,
	 char* name,
	 class Constraint* capacity,
	 const class Type& type);
private:
  /** Copy constructor */
  explicit Place (const class Place& old);
  /** Assignment operator */
  class Place& operator= (const class Place& old);
public:
  /** Destructor */
  ~Place ();

  /** Get the net the place was created in */
  const class Net& getNet () const { return myNet; }
  /** Get the index number of the place */
  unsigned getIndex () const { return myIndex; }
  /** Get the name of the place */
  const char* getName () const { return myName; }
  /** Get the type of the place */
  const class Type& getType () const { return *myType; }

  /** Determine the capacity constraint */
  const class Constraint* getCapacity () const { return myCapacity; }
  /** Determine the number of bits needed for storing the capacity */
  unsigned getCapacityBits () const { return myCapacityBits; }

  /** Get the initial marking expression */
  const class Marking* getInitMarking () const { return myInitMarking; }
  /** Set the initial marking expression */
  void setInitMarking (class Marking& initMarking);
  /** Determine whether this is an implicit place */
  bool isImplicit () const { return myIsImplicit; }
  /** Flag the place visible */
  void flagVisible () { myIsVisible = true; }
  /** Determine whether this place is used in deadlock or reject formulae */
  bool isVisible () const { return myIsVisible; }
  /** Determine whether the place is always nonempty */
  bool isNonempty () const { return myIsNonempty; }
  /** Determine whether the place is constant */
  bool isConstant () const { return myIsConstant; }
  /** Specify whether the place is constant */
  void setConstant (bool constant) { myIsConstant = constant; }
  /** Determine whether the place is suppressed when displaying markings */
  bool isSuppressed () const { return myIsSuppressed; }
  /** Specify whether the place is suppressed when displaying markings */
  void setSuppressed (bool suppressed) const { myIsSuppressed = suppressed; }
  /** Determine the maximum number of tokens the place can contain */
  card_t getMaxNumTokens () const { return myMaxNumTokens; }

  /** Display the place definition
   * @param printer	the printer object
   */
  void display (const class Printer& printer) const;

private:
  /** The net the place was created in */
  const class Net& myNet;
  /** Index number of the place */
  const unsigned myIndex;
  /** Name of the place */
  char* myName;
  /** Capacity constraint */
  class Constraint* myCapacity;
  /** Number of bits required to store the capacity */
  unsigned myCapacityBits;
  /** Type of the place */
  const class Type* myType;
  /** Initial marking of the place */
  class Marking* myInitMarking;
  /** Flag: is this an implicit place */
  bool myIsImplicit;
  /** Flag: is this place used in deadlock or reject formulae */
  bool myIsVisible;
  /** Flag: is this place always nonempty */
  bool myIsNonempty;
  /** Flag: is this place constant */
  bool myIsConstant;
  /** Flag: is this place suppressed when displaying markings */
  mutable bool myIsSuppressed;
  /** Number of tokens always contained in this place (0 if variable) */
  card_t myMaxNumTokens;

  /** The constructor can only be called by Net */
  friend class Net;
};

#endif // PLACE_H_
