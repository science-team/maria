// Arc class -*- c++ -*-

#ifndef ARC_H_
# define ARC_H_
# ifdef __GNUC__
#  pragma interface
# endif // __GNUC__

/** @file Arc.h
 * Labelled arc in a net
 */

/* Copyright � 1998-2003 Marko M�kel� (msmakela@tcs.hut.fi).

   This file is part of MARIA, a reachability analyzer and model checker
   for high-level Petri nets.

   MARIA is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   MARIA is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   The GNU General Public License is often shipped with GNU software, and
   is generally kept in a file called COPYING or LICENSE.  If you do not
   have a copy of the license, write to the Free Software Foundation,
   59 Temple Place, Suite 330, Boston, MA 02111 USA. */

/** Arc between a Place and a Transition labelled with Marking expression */
class Arc
{
public:
  /** Constructor
   * @param output	Flag: is this an output arc?
   * @param expr	Arc expression (formal tokens)
   * @param place	Place associated with the arc
   * @param copy	Flag: is this a copied definition?
   */
  Arc (bool output,
       class Marking& expr,
       const class Place& place,
       bool copy = false);
private:
  /** Copy constructor */
  Arc (const class Arc& old);
  /** Assignment operator */
  class Arc& operator= (const class Arc& old);
public:
  /** Destructor */
  ~Arc ();

  /** Determine whether this is an output arc */
  bool isOutput () const { return myIsOutput; }

  /** Get the arc expression */
  const class Marking& getExpr () const { return *myExpr; }
  /** Substitute the arc expression
   * @param substitution	the variable substitutions
   */
  void substitute (class Substitution& substitution);

  /** Get the place associated with this arc */
  const class Place& getPlace () const { return myPlace; }

  /** Get the index number of the place associated with this arc */
  unsigned getIndex () const { return myIndex; }

  /** Determine whether this is a copied arc definition */
  bool isCopy () const { return myCopy; }

  /** Append an expression to the arc
   * @param expr       expression to be appended
   */
  void append (class Marking& expr);

private:
  /** Flag: is this an output arc? */
  bool myIsOutput;
  /** Arc expression (formal tokens) */
  class Marking* myExpr;
  /** Place associated with the Arc */
  const class Place& myPlace;
  /** Index number of the place in the net */
  const unsigned myIndex;
  /** Flag: is this a copied arc definition? */
  const bool myCopy;
};

#endif // ARC_H_
